## Установка

Подключите репозиторий к приложению. Через хаб будет зарегистрирован новый тип приложения `c2d`.

## Интеграция

Для интеграции будет добавлен новый роут `integrations/c2d/{apiToken}`. Сделайте вот такой GET-запрос:

```http request
GET /integrations/c2d/{apiToken}?api_domain={apiDomain}&tz_offset=3
```

- `apiToken` -- токен вашего аккаунта Chat2Desk
- `apiDomain` -- домен api (https://ror.chat2desk.com)

Будет интегрирован новый пользователь. Однако, *вебхук в локальном приложении не устанавливается*.

### Биллинг

Доступы к биллингу нужно прописать в .env:
- C2D_BILLING_USERNAME
- C2D_BILLING_PASSWORD