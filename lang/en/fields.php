<?php return [
    'regionId' => 'Region id',
    'countryId' => 'Country id',
    'request_id' => 'Request id',
    'channel_id' => 'Channel id',
    'transport' => 'Transport',
    'group_chat_user' => 'Group chat: user',
    'inbox_quote' => 'Inbox: quote',
    'api_request_type' => 'Request type',
    'api_path' => 'Path',
    'api_params' => 'Parameters',
    'api_params_description' => 'For a GET request, the parameters will be placed in the query string, for the rest - in the request body in JSON format.',
    'response_data' => 'Response data',
    'response_is_ok' => 'If response is successful',
    'response_message' => 'Error message',
    'response_validation_errors' => 'List of errors in case the request parameters are incorrect',
    'contact_name' => 'Name',
    'contact_phone' => 'Phone',
    'contact_comment' => 'Comment',
    'contact_external_service' => 'External service',
    'contact_external_id' => 'External id',
];