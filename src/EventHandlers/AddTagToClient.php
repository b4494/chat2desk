<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class AddTagToClient
{
    public function __invoke(DataWrap $data): array
    {
        $tag = new Tag($data['id'], $data['name'], $data['group_name']);

        return array_map(fn ($chat) => $this->generateEvent($data, $chat, $tag), $this->chats($data));
    }

    protected function generateEvent(DataWrap $data, Chat $chat, $tagId): Event
    {
        return new AttachedTagToChat($chat, $tagId, timestamp: $data->timestamp());
    }

    /** @return array<Chat> */
    protected function chats(DataWrap $data): array
    {
        return Cache::remember('c2d_cl_d:'.$data['client_id'], Carbon::now()->addHour(), function () use ($data) {
            $apiCommands = $data->module->getApiCommands();

            $dialogs = Arr::pluck($apiCommands->clientDialogs($data['client_id']), 'id');

            // For single dialog and tag attached event we simply go on
            if (count($dialogs) == 1 && isset($data['channel_id'])) {
                return [ (new DataWrap($data->module, [
                    'dialog_id' => $dialogs[0],
                    'client_id' => $data['client_id'],
                    'channel_id' => $data['channel_id'],
                    'transport' => $data['transport'],
                ]))->chat() ];
            }

            return Cache::remember(
                'c2d_chats:'.implode(',', $dialogs),
                Carbon::now()->addDay(),
                fn() => array_map(fn($id) => $this->chatFromDialogId($data, $id), $dialogs),
            );
        });
    }

    protected function chatFromDialogId(DataWrap $data, int $dialogId): Chat
    {
        $messages = $data->module->getApiCommands()->messages([
            'client_id' => $data['client_id'],
            'dialog_id' => $dialogId,
            'order' => 'desc',
            'limit' => 1,
        ]);

        $rawData = Arr::only($messages[0], [ 'client_id', 'dialog_id', 'transport', 'channel_id' ]);

        return (new DataWrap($data->module, $rawData))->chat();
    }
}