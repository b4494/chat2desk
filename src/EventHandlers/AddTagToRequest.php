<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;

class AddTagToRequest
{
    public function __invoke(DataWrap $data): Event
    {
        return new AttachedTagToChat(
            chat: $data->chat(),
            tag: $data['id'],
            forTicket: true,
            timestamp: $data->timestamp(),
        );
    }
}