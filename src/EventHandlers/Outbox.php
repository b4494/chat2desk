<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Chat2Desk\Utils\DataWrap;

class Outbox extends Inbox
{
    public function __invoke(DataWrap $data): ?Event
    {
        if ($data['type'] != 'to_client') return null;

        return new OutboxSent(
            chat: $data->chat(),
            message: $this->messageData($data),
            operator: (string)$data['operator_id'],
            timestamp: $data->timestamp(),
        );
    }
}