<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Events\DetachedTagFromChat;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;

class DeleteTagFromClient extends AddTagToClient
{
    protected function generateEvent(DataWrap $data, Chat $chat, $tagId): Event
    {
        return new DetachedTagFromChat($chat, $tagId, timestamp: $data->timestamp());
    }
}