<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Events\NewContactChatAdded;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use Illuminate\Support\Arr;

class NewClient extends ClientUpdated
{
    public function __invoke(DataWrap $data): ?Event
    {
        if (!in_array($data['client_source'], [ 'write_first', 'AMO' ])) return null;

        // Get channel from the event
        if (!$channel = Arr::first($data['channels'])) {
            throw new ErrorException(ErrorCode::DataMissing, 'Missing channel');
        }

        // Get dialogs of the client, since we don't know the dialog from event
        if (!$dialogs = $data->module->getApiCommands()->clientDialogs($data['id'])) {
            throw new ErrorException(ErrorCode::DataMissing, 'Missing dialogs');
        }

        // We construct new data to be able to resolve chat
        $newData = new DataWrap($data->module, [
            'client_id' => $data['id'],
            'dialog_id' => $dialogs[0]['id'],
            'operator_id' => $dialogs[0]['operator_id'],
            'channel_id' => $channel['id'],
            'transport' => Arr::first($channel['transports']),
            'client' => [ 'phone' => $data['phone'] ],
        ]);

        return new NewContactChatAdded(
            chat: $newData->chat(),
            contact: $this->contact($data),
            extraData: [ ExtraDataProps::CLIENT_SOURCE => $data['client_source'] ],
            timestamp: $data->timestamp(),
        );
    }
}