<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Events\ContactDataUpdated;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;

class ClientUpdated
{
    public function __invoke(DataWrap $data): ?Event
    {
        return new ContactDataUpdated($this->contact($data), timestamp: $data->timestamp());
    }

    /**
     * @param \BmPlatform\Chat2Desk\Utils\DataWrap $data
     *
     * @return \BmPlatform\Abstraction\DataTypes\Contact
     */
    protected function contact(DataWrap $data): Contact
    {
        return new Contact(
            externalId: $data['id'],
            name: $data['assigned_name'] ?: $data['name'],
            phone: Helpers::extractPhoneNumber(($data['client_phone'] ?? null) ?: $data['phone']),
            avatarUrl: $data['avatar'],
            extraFields: isset($data['custom_fields']) ? ($data['custom_fields'] + [ ExtraDataProps::COMMENT => $data['comment'] ]) : null,
            extraData: Helpers::withoutEmptyStrings([
                ExtraDataProps::COUNTRY_ID => $data['country_id'],
                ExtraDataProps::REGION_ID => $data['region_id'],
                ExtraDataProps::CLIENT_PHONE => $data['phone'],
            ]),
        );
    }
}