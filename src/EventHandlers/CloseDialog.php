<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;

class CloseDialog
{
    public function __invoke(DataWrap $data): Event
    {
        return new ChatTicketClosed($data->chat(), (string)$data['request_id'], $data->timestamp());
    }
}