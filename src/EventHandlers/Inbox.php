<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\ExternalPlatformItem;
use BmPlatform\Abstraction\DataTypes\GeoLocation;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\EventHandler;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;
use Illuminate\Support\Str;

class Inbox
{
    const TEXT_REGEXP = '/^(\((?<groupUser>.+?)\)\s)?([«"](?<quote>.+)[»"]\n([-—]\s)?)?(?<text>.+)$/su';

    public function __invoke(DataWrap $data): ?Event
    {
        if ($this->isClassifiedOutboxMessage($data)) {
            return $this->classifiedOutbox($data);
        }

        return new InboxReceived(
            chat: $data->chat(),
            participant: new Contact(
                externalId: (string)$data['client_id'],
                name: $data['client']['assigned_name'] ?: $data['client']['name'],
                phone: Helpers::extractPhoneNumber($data['client']['client_phone'] ?: $data['client']['phone']),
                extraData: [ ExtraDataProps::CLIENT_PHONE => $data['client']['phone']],
            ),
            message: $this->messageData($data),
            externalItem: $this->socialPost($data),
            flags: $this->flags($data) ?: null,
            timestamp: $data->timestamp(),
        );
    }

    protected function messageData(DataWrap $data, bool $isClassifiedOut = false): MessageData
    {
        [ $text, $extraData ] = $this->parseText($data['text'], $isClassifiedOut);

        return new MessageData(
            externalId: (string)$data['message_id'],
            text: $text ?: null,
            attachments: $this->attachments($data),
            geoLocation: $this->geoLocation($data),
            extraData: $extraData,
        );
    }

    protected function parseText(?string $text, bool $isClassifiedOut): array
    {
        if (!$text) return [ null, null ];

        if ($isClassifiedOut) {
            if (!Str::contains($text, PHP_EOL)) return [ null, null ];

            [,$text] = explode(PHP_EOL, $text, 2);

            return [ $text, null ];
        }

        if (!preg_match(self::TEXT_REGEXP, $text, $matches, PREG_UNMATCHED_AS_NULL)) {
            return [ trim($text), null ];
        }

        if (isset($matches['groupUser'])) $extraData[ExtraDataProps::GROUP_CHAT_USER] = $matches['groupUser'];
        if (isset($matches['quote'])) $extraData[ExtraDataProps::QUOTED_TEXT] = $matches['quote'];

        return [ $matches['text'], $extraData ?? null ];
    }

    protected function attachments(DataWrap $data): ?array
    {
        return collect($data['attachments'] ?? null)
            ->map(fn ($i) => new MediaFile(
                type: MediaFileType::fromMime($i['content_type']),
                url: $i['file']['url'],
                mime: $i['content_type'],
            ))->all() ?: null;
    }

    protected function socialPost(DataWrap $data): ?ExternalPlatformItem
    {
        if (($extraData = $data['extra_data'] ?? null) && isset($extraData['post_id'])) {
            return new ExternalPlatformItem($extraData['post_id'], $extraData['post_url'] ?? null);
        }

        return null;
    }

    protected function flags(DataWrap $data): int
    {
        $flags = 0;

        if ($data['is_new_request']) $flags |= InboxFlags::NEW_TICKET_OPENED;
        if ($data['is_new_client']) $flags |= InboxFlags::NEW_CHAT_CREATED;
        if ($data['insta_comment']) $flags |= InboxFlags::EXTERNAL_POST_COMMENT;

        return $flags;
    }

    protected function geoLocation(DataWrap $data): ?GeoLocation
    {
        if (!$data['coordinates']) return null;

        [ $lat, $long ] = explode(' ', $data['coordinates'], 2);

        return new GeoLocation($lat, $long);
    }

    private function isClassifiedOutboxMessage(DataWrap $data): bool
    {
        return $data['text'] && Str::startsWith($data['text'], '↗️ Пишет');
    }

    private function classifiedOutbox(DataWrap $data): Event
    {
        return new OutboxSent($data->chat(), $this->messageData($data), timestamp: $data->timestamp());
    }
}