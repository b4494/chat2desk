<?php

namespace BmPlatform\Chat2Desk\EventHandlers;

use BmPlatform\Abstraction\Events\ChatTransferredToOperator;
use BmPlatform\Abstraction\Events\Event;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\EventHandler;

class DialogTransferred
{
    public function __invoke(DataWrap $data): Event
    {
        return new ChatTransferredToOperator(
            chat: $data->chat(),
            newOperator: (string)$data['current_operator_id'],
            prevOperator: ($v = $data['last_operator_id']) ? (string)$v : null,
            timestamp: $data->timestamp(),
        );
    }
}