<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\DataTypes\OperatorGroup;
use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\AppHandler as AppHandlerInterface;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Features\HasOperatorGroups;
use BmPlatform\Abstraction\Interfaces\Features\HasOperators;
use BmPlatform\Abstraction\Interfaces\Features\HasTags;
use BmPlatform\Abstraction\Interfaces\Features\SupportsOneTimePayments;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Support\Facades\App;
use RuntimeException;

class AppHandler implements AppHandlerInterface, HasOperatorGroups, HasOperators, HasTags, SupportsOneTimePayments
{
    public function __construct(
        public    readonly AppInstance            $user,
        protected readonly Repository             $config,
        protected readonly BillingCommands        $billingCommands,
        private ?Commands                         $commands = null,
        private ?ApiCommands                      $apiCommands = null,
        private ?ApiClient                        $apiClient = null,
    ) {
        //
    }

    public function getMessengerInstances(mixed $cursor = null): IterableData
    {
        return new IterableData(collect($this->getApiClient()->get('channels'))->flatMap(function (array $data) {
            /** @var array{id: int, name: string, transports: array, phone: string} $data */
            $transports = array_intersect($data['transports'], $this->allowedTransports());

            return collect($transports)->map(fn(string $transport) => new MessengerInstance(
                externalType: $transport,
                externalId: "{$data['id']}:{$transport}",
                name: count($transports) > 1 ? $data['name'].' '.$this->config->get('c2d.messenger_types.'.$transport.'.name') : $data['name'],
                internalType: with($this->config->get('c2d.messenger_types.'.$transport.'.internal_type'), fn ($v) => $v ? MessengerType::tryFrom($v) : null),
            ));
        })->all(), null);
    }

    public function getOperatorGroups(mixed $cursor = null): IterableData
    {
        return with($this->getApiClient()->all('operators_groups', $cursor), fn (IterableData $data) => new IterableData(
            collect($data->data)
            ->filter(fn($gr) => $gr['operator_ids'])
            ->map(fn($gr) => new OperatorGroup($gr['id'], $gr['name']))
            ->all(), $data->cursor)
        );
    }

    public function getOperators(mixed $cursor = null): IterableData
    {
        return with($this->getApiClient()->all('operators', $cursor), fn (IterableData $data) => new IterableData(
            collect($data->data)
            ->filter(fn($op) => !in_array($op['role'], [ 'deleted', 'disabled' ]))
            ->map(fn($op) => new Operator(
                externalId: $op['id'],
                name: trim("{$op['first_name']} {$op['last_name']}"),
                email: $op['email'],
                avatarUrl: $op['avatar'],
            ))->all(), $data->cursor)
        );
    }

    public function getCommands(): \BmPlatform\Abstraction\Interfaces\Commands\Commands
    {
        if (isset($this->commands)) return $this->commands;

        return $this->commands = new Commands($this);
    }

    public function getApiClient(): ApiClient
    {
        if (isset($this->apiClient)) return $this->apiClient;

        [ $server, ] = Utils::parseExternalId($this->user->getExternalId());

        if (!$domain = $this->config->get("c2d.servers.{$server}.api_domain")) {
            throw new RuntimeException("Chat2Desk server [$server] is not configured.");
        }

        return $this->apiClient = new ApiClient($domain, $this->user->getExtraData()[ExtraDataProps::TOKEN], [
            'appInstance' => $this->user,
        ]);
    }

    public function getTags(mixed $cursor = null): IterableData
    {
        return with($this->getApiClient()->all('tags', $cursor), fn (IterableData $data) => new IterableData(
            collect($data->data)
                ->map(fn($data) => new Tag($data['id'], $data['label'], $data['group_name']))
                ->all()
        , $data->cursor));
    }

    public function getApiCommands(): ApiCommands
    {
        if (isset($this->apiCommands)) return $this->apiCommands;

        return $this->apiCommands = new ApiCommands($this->getApiClient());
    }

    public function allowedTransports(): array
    {
        return array_keys($this->config->get('c2d.messenger_types'));
    }

    public function activate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))();
    }

    public function deactivate(): void
    {
        (new SetWebhook($this, App::make(Hub::class)))(enabled: false);
    }

    public function status(): AppExternalStatus
    {
        // Check if we have access
        $this->getApiClient()->get('companies/api_info');

        if ($billingId = $this->user->getExtraData()[ExtraDataProps::BILLING_ID] ?? null) {
            $status = $this->billingCommands->resolveCompanyStatus($billingId);
        }

        return ($status ?? null) ?: new AppExternalStatus(AppStatus::Active);
    }

    public function getPaymentForServices(array $items): mixed
    {
        if (!$companyId = $this->user->getExtraData()[ExtraDataProps::BILLING_ID] ?? null) {
            throw new ErrorException(ErrorCode::DataMissing, 'Billing is not available.');
        }

        $this->billingCommands->getPaymentForServices($companyId, $items);

        return null;
    }
}