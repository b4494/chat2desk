<?php

namespace BmPlatform\Chat2Desk\Utils;

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\AppHandler;
use Carbon\Carbon;

class DataWrap implements \ArrayAccess
{
    public function __construct(public readonly AppHandler $module, public readonly array $data)
    {
        //
    }

    public function messengerInstance(): string
    {
        $channelId = $this->data['channel_id']
            ?: throw new ErrorException(ErrorCode::DataMissing, 'Missing [channel_id] value');

        $transport = $this->data['transport']
            ?: throw new ErrorException(ErrorCode::DataMissing, 'Missing [transport] value');

        return "$channelId:$transport";
    }

    public function chat(): Chat
    {
        $extraData = [
            ExtraDataProps::CLIENT_ID => $this->data['client_id']
                ?? throw new ErrorException(ErrorCode::DataMissing, 'Missing [client_id] value'),
        ];

        if (isset($this->data['request_id'])) {
            $extraData[ExtraDataProps::REQUEST_ID] = $this->data['request_id'];
        }

        return new Chat(
            externalId: (string)$this->dialogId(),
            messengerInstance: $this->messengerInstance(),
            contact: $this->data['client_id'],
            operator: $this->data['operator_id'] ?? null,
            messengerId: $this->parseMessengerId($this->data['client']['phone'] ?? null),
            extraData: $extraData,
        );
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    public function dialogId(): int
    {
        if ($value = $this->data['dialog_id'] ?? null) {
            return (int)$value;
        }

        if (!isset($this->data['request_id'])) {
            throw new ErrorException(ErrorCode::DataMissing, 'Failed to resolve [dialog_id]');
        }

        return $this->module->getApiCommands()->request($this->data['request_id'])['dialog_id'];
    }

    protected function parseMessengerId($value): ?string
    {
        if (!$value || !preg_match('/^\[.+]\s(?<id>.+)$/', $value, $matches)) return null;

        return $matches['id'];
    }

    public function timestamp(): Carbon
    {
        return Carbon::parse($this->data['event_time']);
    }

    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset): mixed
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset): void
    {
        // TODO: Implement offsetUnset() method.
    }
}