<?php

namespace BmPlatform\Chat2Desk\Utils;

use BmPlatform\Abstraction\Enums\MessengerType;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use Illuminate\Support\Facades\Config;

final class Utils
{
    public static function parseExternalId(string $externalId): array
    {
        return explode(':', $externalId, 2);
    }

    /** @return array{channel_id: int, transport: string} */
    public static function channelTransportParams(MessengerInstance $messenger): array
    {
        [ $channel_id, $transport ] = explode(':', $messenger->getExternalId(), 2);

        return compact('channel_id', 'transport');
    }

    public static function matchServer($apiUrl): ?string
    {
        foreach (Config::get('c2d.servers') as $id => $server) {
            if (preg_match("#^{$server['matcher']}$#", $apiUrl)) {
                return $id;
            }
        }

        return null;
    }

    public static function customFieldVar(array $config): array
    {
        $name = $config['name'] ?: $config['id'];
        $key = 'contact.customFields.'.str_replace('.', ' ', $name);

        return [ $name, $key ];
    }
}