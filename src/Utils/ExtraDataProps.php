<?php

namespace BmPlatform\Chat2Desk\Utils;

class ExtraDataProps
{
    const CLIENT_ID = 'clid';
    const REQUEST_ID = 'reqid';
    const OPERATOR_ID = 'opid';
    const GROUP_CHAT_USER = 'gcu';
    const QUOTED_TEXT = 'quote';
    const COUNTRY_ID = 'ctid';
    const REGION_ID = 'rgid';
    const CLIENT_PHONE = 'clph';
    const CLIENT_SOURCE = 'clsr';
    const ADMIN_OPERATOR_ID = 'admid';
    const COMMENT = 'cm';
    const CUSTOM_FIELDS = 'customFields';
    const TOKEN = 'token';
    const BILLING_ID = 'billingId';
}