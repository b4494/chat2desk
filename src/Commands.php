<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Events\ChatTicketClosed;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Commands\Commands as CommandsInterface;
use BmPlatform\Abstraction\Interfaces\Commands\SendsSystemMessages;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTags;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsChatTickets;
use BmPlatform\Abstraction\Interfaces\Commands\SupportsCustomActions;
use BmPlatform\Abstraction\Interfaces\Commands\TransfersChatToOperator;
use BmPlatform\Abstraction\Interfaces\Commands\UpdatesContactData;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Chat2Desk\Commands\MessageCommands;
use BmPlatform\Chat2Desk\Commands\OperatorCommands;
use BmPlatform\Chat2Desk\Commands\TagCommands;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Exceptions\ValidationException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Support\Helpers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

class Commands implements CommandsInterface, SendsSystemMessages, UpdatesContactData, TransfersChatToOperator,
                          SupportsChatTags, SupportsChatTickets, SupportsCustomActions
{
    use MessageCommands, OperatorCommands, TagCommands;

    public function __construct(public readonly AppHandler $module)
    {
        //
    }

    public function updateContactData(UpdateContactDataRequest $request): ?Contact
    {
        $payload = [];

        foreach ($request->data as $key => $value) {
            if (str_starts_with($key, 'customField')) {
                $customFieldId = substr($key, 11);

                $payload['custom_fields'][$customFieldId] = $value;
            } elseif ($key == 'assigned_phone' && $value) {
                if (!$value = Helpers::extractPhoneNumber($value)) {
                    throw new ErrorException(ErrorCode::UnexpectedData, 'Phone number must be a valid phone number in international format');
                }

                $payload[$key] = $value;
            } else {
                $payload[$key] = $value;
            }
        }

        if (!$payload) return null;

        $this->module->getApiClient()->put('clients/:clientId', [
            'params' => [ 'clientId' => $request->contact->getExternalId() ],
            'json' => $payload,
        ]);

        $ct = $request->contact;

        return new Contact(
            externalId: $ct->getExternalId(),
            name: $payload['nickname'] ?? $ct->getName(),
            phone: $payload['assigned_phone'] ?? $ct->getPhone(),
            avatarUrl: $ct->getAvatarUrl(),
            extraFields: ($payload['custom_fields'] ?? []) +
            (isset($payload['comment']) ? [ ExtraDataProps::COMMENT => $payload['comment'] ] : []) +
            $ct->getExtraFields(),
        );
    }

    public function closeChatTicket(Chat $chat): void
    {
        $data = ($apiClient = $this->module->getApiClient())->get('dialogs/:id', [
            'params' => [ 'id' => $chat->getExternalId() ],
        ]);

        if ($data['state'] == 'closed') {
            throw new ErrorException(ErrorCode::ChatTicketAlreadyClosed);
        }

        // Assign to admin if no operator provided
        if (!isset($data['operator_id'])) {
            $data['operator_id'] = $this->module->user->getExtraData()[ExtraDataProps::ADMIN_OPERATOR_ID];
        }

        $apiClient->put('dialogs/:id', [
            'params' => [ 'id' => $chat->getExternalId() ],
            'json' => [ 'state' => 'closed', 'operator_id' => $data['operator_id'] ],
        ]);

        Event::dispatch(new ChatTicketClosed($this->module->user, $chat->getExternalId(), timestamp: Carbon::now()));
    }

    public function callCustomAction(RuntimeContext $context, string $id, array $args): ?array
    {
        return match ($id) {
            'c2dApiCall' => $this->customApiCall($context, $args),
            default => throw new ErrorException(ErrorCode::UnexpectedData, 'Unknown custom action '.$id),
        };
    }

    private function customApiCall(RuntimeContext $context, array $args): array
    {
        if (!isset($args['type']) || !in_array($type = strtoupper($args['type']), [ 'GET', 'POST', 'PUT', 'DELETE' ])) {
            throw new ErrorException(ErrorCode::UnexpectedData, 'Invalid request type provided');
        }

        if (!isset($args['path'])) throw new ErrorException(ErrorCode::UnexpectedData, 'Missing path');

        try {
            return [
                'ok' => true,
                'data' => $this->module->getApiClient()->request($type, $args['path'], [
                    ...($type === 'GET' ? [ 'query' => $args['params'] ?? null ] : []),
                    ...($type !== 'GET' ? [ 'json' => $args['params'] ?? null ] : []),
                ]),
            ];
        }

        catch (ValidationException $e) {
            return [
                'ok' => false,
                'message' => $e->getMessage(),
                'errors' => $e->errors->all(),
            ];
        }

        catch (ErrorException $e) {
            if ($e->errorCode != ErrorCode::UnexpectedServerError) throw $e;

            return [
                'ok' => false,
                'message' => $e->getMessage(),
            ];
        }
    }
}