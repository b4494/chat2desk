<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\Tag;
use BmPlatform\Abstraction\Enums\VariableCategory;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\Abstraction\Interfaces\RuntimeContext;
use BmPlatform\Abstraction\Interfaces\VariableRegistrar;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;
use BmPlatform\Support\Http\HttpClient;
use Illuminate\Config\Repository;
use Illuminate\Support\Arr;
use Lazychaser\CdBilling\Client;
use Lazychaser\CdBilling\CompanyApi;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/c2d.php', 'c2d');
        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');
        $this->loadTranslationsFrom(__DIR__.'/../lang', 'c2d');

        /** @var \Illuminate\Config\Repository $config */
        $config = $this->app->make('config');

        $this->makeHub()->registerAppType(
            type: 'c2d',
            createHandler: static fn (AppInstance $app) => new AppHandler(
                user: $app,
                config: $config,
                billingCommands: self::makeBillingCommands($app, $config)
            ),
            options: [
                'schema' => static fn () => $config->get('c2d'),
                'registerVariables' => $this->registerVariables(),
                'registerInstanceVariables' => $this->registerInstanceVariables(),
            ]
        );
    }

    public function register(): void
    {
        $this->app->singleton(BillingCommands::class, function ($app) {
            return static::makeBillingCommands(null, $app->make('config'));
        });
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeHub(): Hub
    {
        return $this->app->make(Hub::class);
    }

    /**
     * @return \Closure
     */
    protected function registerVariables(): \Closure
    {
        return static function (VariableRegistrar $registrar) {
            $registrar->mixed('inboxQuote')
                ->category('event')
                ->contextFlags([ 'clientResponse' ])
                ->name(trans('c2d::fields.inbox_quote'));

            $registrar->text('groupChatUser')
                ->category('event')
                ->contextFlags([ 'clientResponse' ])
                ->name(trans('c2d::fields.group_chat_user'));

            $registrar->int('contact.regionId',static fn(Contact $c) => $c->getExtraData()[ExtraDataProps::REGION_ID])
                ->name(trans('c2d::fields.region_id'));

            $registrar->int('contact.countryId', static fn(Contact $c) => $c->getExtraData()[ExtraDataProps::COUNTRY_ID])
                ->name(trans('c2d::fields.country_id'));

            $registrar->mixed('contact.comment', static fn(Contact $c) => $c->getExtraFields()[ExtraDataProps::COMMENT]);

            $registrar->text('transport',
                static fn(RuntimeContext $c) => $c->chat()->getMessengerInstance()->getExternalType()
            )->category(VariableCategory::chat)->name(trans('c2d::fields.transport'));

            $registrar->int(
                'channelId',
                static fn(RuntimeContext $c) => Utils::channelTransportParams($c->chat()->getMessengerInstance())['channel_id']
            )->category('chat')->name(trans('c2d::fields.channel_id'));

            $registrar->pk('requestId',
                static fn (RuntimeContext $c) => $c->chat()->getExtraData()[ExtraDataProps::REQUEST_ID]
            )->hideFromConditions()->name(trans('c2d::fields.request_id'));

            $registrar->alias([
                'operatorId' =>  'operator.externalId',
                'comment' => 'contact.comment',
                'clientDialogId' => 'dialogId',
                'clientMessageId' => 'messageId',
                'operatorOnline' => 'operator.isOnline',
                'operator_online' => 'operator.isOnline',
                'clientName' => 'contact.name',
                'client_name' => 'contact.name',
                'clientPhone' => 'contact.phone',
                'client_phone' => 'contact.phone',
                'newRequest' => 'newTicketOpened',
                'newClient' => 'newChatCreated',
            ]);
        };
    }

    protected function registerInstanceVariables(): callable
    {
        return function (VariableRegistrar $registrar, AppInstance $instance) {
            /** @var \BmPlatform\Chat2Desk\ApiCommands $commands */
            $commands = $instance->getHandler()->getApiCommands();

            $registrar->tag('contact.tags', static function (Contact $client, RuntimeContext $ctx) use ($commands) {
                return $ctx->cacheValue('tags',
                    static fn () => collect($commands->client($client->getExternalId())['tags'])
                        ->map(static fn ($data) => new Tag($data['id'], $data['label'], $data['group_name']))
                        ->all()
                );
            })->array();

            $registrar->bool('operator.isOnline', static function (Operator $operator) use ($commands) {
                $operator = $commands->operatorByEmail($operator->getEmail());

                return $operator && $operator['online'] && !$operator['offline_type'];
            });

            $registrar->complex('contact.customFields', function (VariableRegistrar $_registrar) use ($instance, $registrar) {
                foreach (Arr::wrap($instance->getExtraData()['customFields'] ?? []) as $field) {
                    [$name, $key] = Utils::customFieldVar($field);

                    $_registrar
                        ->mixed(mb_substr($key, 21), static fn ($fields) => $fields[$field['id']] ?? null)
                        ->name($name);

                    $registrar->alias("custom_field{$field['id']}", 'contact.customFields.'.$key);
                    $registrar->alias("customField{$field['id']}", 'contact.customFields.'.$key);
                }
            }, static fn (Contact $contact) => $contact->getExtraFields());
        };
    }

    protected static function makeBillingCommands(?AppInstance $app, Repository $configRepo): BillingCommands
    {
        $config = $configRepo->get('c2d.billing');

        // Provide default handler stack and app instance
        $clientOptions = array_merge($config['options'], [
            'handler' => HttpClient::createHandlerStack(),
            'appInstance' => $app,
        ]);

        $client = new Client($config['username'], $config['password'], $clientOptions, $config['domain']);

        return new BillingCommands(new CompanyApi($client), $config);
    }
}