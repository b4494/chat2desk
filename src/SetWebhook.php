<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\Interfaces\Hub;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;

class SetWebhook
{
    const EVENTS = [
        'inbox', 'outbox', 'new_client', 'close_dialog', 'client_updated',
        'add_tag_to_client', 'add_tag_to_request', 'delete_tag_from_client', 'delete_tag_from_request',
        'dialog_transferred',
    ];

    public function __construct(public readonly AppHandler $handler, public readonly Hub $hub)
    {
        //
    }

    public function __invoke($enabled = true)
    {
        if (App::isLocal()) return;

        $url = URL::route('c2d.callback', [ 'callbackId' => $this->hub->getCallbackId($this->handler->user) ]);

        $apiClient = $this->handler->getApiClient();

        $webhook = collect($apiClient->get('webhooks'))->firstWhere('url', $url);

        if ($webhook && !$this->shouldUpdate($webhook, $enabled)) return;

        $data = [
            'name' => 'Sales Tunnels',
            'url' => $url,
            'events' => self::EVENTS,
            'status' => $enabled ? 'enable' : 'disable',
        ];

        if ($webhook) {
            $apiClient->put('webhooks/:id', [ 'params' => [ 'id' => $webhook['id'] ], 'json' => $data ]);
        } else {
            $apiClient->post('webhooks', [ 'json' => $data ]);
        }
    }

    private function shouldUpdate(array $webhook, bool $enabled): bool
    {
        return $webhook['status'] != ($enabled ? 'enable' : 'disable')
            || array_diff(self::EVENTS, $webhook['events'])
            || array_diff($webhook['events'], self::EVENTS);
    }
}