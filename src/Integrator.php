<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\AppIntegrationData;
use BmPlatform\Abstraction\Enums\AccountType;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentType;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;
use Brick\Money\Currency;
use Carbon\CarbonTimeZone;
use Illuminate\Support\Facades\Config;

class Integrator
{
    public function __construct(
        public readonly array $data,
        public readonly BillingCommands $billingStatusResolver,
        public readonly bool $forCatalog = false,
    ) {
        //
    }

    public function __invoke(): AppIntegrationData
    {
        $apiClient = $this->createApiClient();
        $info = $apiClient->get('companies/api_info');

        if ($info['company_mode'] == 'demo') {
            throw new ErrorException(ErrorCode::IntegrationNotPossible, 'Demo companies are not allowed to be integrated');
        }

        $serverId = $this->serverId();
        $server = Config::get('c2d.servers.'.$serverId);
        $billingId = $server['region_id'].'-'.$info['companyID'];
        $billingStatus = $this->billingStatusResolver->resolveCompanyStatus($billingId);

        return new AppIntegrationData(
            type: 'c2d',
            externalId: $serverId.':'.$info['companyID'].($this->forCatalog ? ':catalog' : ''),
            name: $info['company_name'],
            locale: $info['company_lang'],
            timeZone: CarbonTimeZone::createFromHourOffset($this->data['tz_offset']),
            email: $info['admin_email'],
            extraData: [
                ExtraDataProps::TOKEN => $this->data['token'],
                'writeFirst' => $info['write_first_option'],
                ExtraDataProps::CUSTOM_FIELDS => $this->customFields($apiClient),
                ExtraDataProps::BILLING_ID => $billingId,
            ],
            paymentType: $billingStatus ? PaymentType::External : PaymentType::Internal,
            status: $this->forCatalog
                ? ($billingStatus ? new AppExternalStatus(AppStatus::Active) : new AppExternalStatus(AppStatus::Disabled))
                : $billingStatus,
            accountType: $this->forCatalog ? AccountType::CatalogOnly : AccountType::Full,
            subPartnersChain: [ $server['region_id'], $server['region_id'].'_'.$info['partnerID'] ],
            currency: isset($this->billingStatusResolver->currency)
                ? Currency::of($this->billingStatusResolver->currency)->getCurrencyCode()
                : null,
        );
    }

    protected function createApiClient(): ApiClient
    {
        return new ApiClient($this->data['api_domain'], $this->data['token']);
    }

    protected function serverId(): string
    {
        if ($server = Utils::matchServer($this->data['api_domain'])) return $server;

        throw new ErrorException(ErrorCode::IntegrationNotPossible, 'Api server is not registered');
    }

    protected function customFields(ApiClient $apiClient): array
    {
        if (!is_array($value = $apiClient->get('custom_client_fields'))) return [];

        return collect($value)->map(fn ($i) => [
            'id' => $i['id'],
            'name' => $i['name'],
            'type' => $i['type'] == 'dropdown' ? 'options' : 'input',
            ...($i['type'] === 'dropdown' ? [ 'options' => explode(',', $i['value']) ] : []),
        ])->all();
    }
}