<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\DataTypes\ReceiptItem;
use BmPlatform\Abstraction\Enums\AppStatus;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\PaymentStatus;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use Brick\Math\BigDecimal;
use Brick\Money\Money;
use Illuminate\Support\Collection;
use Lazychaser\CdBilling\BillingClientException;
use Lazychaser\CdBilling\CompanyApi;
use Lazychaser\CdBilling\DataTypes\ServiceMode;
use Lazychaser\CdBilling\DataTypes\ServiceStatus;

class BillingCommands
{
    public int $currency;

    public function __construct(protected readonly CompanyApi $companyApi, protected readonly array $config)
    {
        //
    }

    public function resolveCompanyStatus(string $billingCompanyId): ?AppExternalStatus
    {
        try {
            $result = $this->companyApi->activeServices($billingCompanyId);

            $this->currency = $result->currency;
        } catch (BillingClientException) {
            return null;
        }

        $possibleServiceIds = $this->config['serviceIds'];

        foreach ($result->services as $service) {
            if (!in_array($service->serviceTypeId, $possibleServiceIds)) continue;

            return new AppExternalStatus(
                status: AppStatus::Active,
                paymentStatus: match ($service->status) {
                    ServiceStatus::active => PaymentStatus::Paid,
                    ServiceStatus::suspended => PaymentStatus::NotPaid,
                },
                externalTariffId: $service->serviceTypeId,
            );
        }

        return new AppExternalStatus(AppStatus::Active);
    }

    /** @var array<\BmPlatform\Abstraction\DataTypes\ReceiptItem> $items */
    public function getPaymentForServices(string $companyId, array $items, ?int $ts = null): void
    {
        if (!$items) return;

        $this->validateItemsCurrency($companyId, $items);
        $serviceTypeId = $this->validateServiceTypeId($items);
        $total = $this->validateBalance($companyId, $items);

        try {
            $this->companyApi->activateService($companyId, [
                'serviceTypeId' => $serviceTypeId,
                'serviceInstanceId' => $ts = $ts ?: (int)(microtime(true) * 1000),
                'quantity' => $total->getAmount()->toInt(),
                'serviceMode' => ServiceMode::standard,
                'eventDate' => $ts,
            ]);
        }

        catch (BillingClientException $e) {
            throw new ErrorException(ErrorCode::BadRequest, $e->getMessage());
        }
    }

    /**
     * @param string $companyId
     * @param array $items
     */
    protected function validateItemsCurrency(string $companyId, array $items): void
    {
        $accountCurrency = $this->companyApi->activeServices($companyId)->currency;

        foreach ($items as $item) {
            if ($item->amount->getCurrency()->getNumericCode() != $accountCurrency) {
                throw new ErrorException(ErrorCode::UnexpectedData, sprintf(
                    'Currency for item [%s] is [%d] but account currency is [%d]',
                    $item->id,
                    $item->amount->getCurrency()->getNumericCode(),
                    $accountCurrency,
                ));
            }
        }
    }

    /** @throws \BmPlatform\Chat2Desk\Exceptions\ErrorException */
    protected function validateBalance(string $companyId, array $items): Money
    {
        $balanceInfo = $this->companyApi->balance($companyId);

        /** @var Money $total */
        $total = array_reduce($items, fn(?Money $carry, ReceiptItem $item) => $item->amount
            ->multipliedBy($item->quantity)
            ->plus($carry ?: 0)
        );

        if ($total->getAmount()->hasNonZeroFractionalPart()) {
            throw new ErrorException(
                ErrorCode::UnexpectedData,
                'Total amount of payment cannot have minor units.',
            );
        }

        if (BigDecimal::of($balanceInfo->currentBalance)
            ->minus($total->getAmount())
            ->isLessThan($balanceInfo->thresholdTurnoffValue)
        ) {
            throw new ErrorException(ErrorCode::InsufficientFunds);
        }

        return $total;
    }

    protected function validateServiceTypeId(array $items): int
    {
        $groupSet = false;
        $group = null;

        foreach ($items as $item) {
            if (!$groupSet) {
                $group = $item->externalId;
                $groupSet = true;
            } elseif ($group != $item->externalId) {
                throw new ErrorException(
                    ErrorCode::UnexpectedData,
                    'Cannot get payment for different services. External id must not be specified or be same for every item.'
                );
            }
        }

        return $group ?: $this->config['defaultOneTimeServiceId'];
    }
}