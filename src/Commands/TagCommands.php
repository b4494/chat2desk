<?php

namespace BmPlatform\Chat2Desk\Commands;

use BmPlatform\Abstraction\Interfaces\Tag;
use BmPlatform\Abstraction\Requests\ChatTagRequest;
use BmPlatform\Chat2Desk\Exceptions\ValidationException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;

trait TagCommands
{
    public function attachTagToChat(ChatTagRequest $request): void
    {
        $this->module->getApiClient()->post('tags/assign_to', [
            'json' => [
                'tag_ids' => [ $this->tagId($request->tag) ],
                'assignee_type' => $request->forTicket ? 'request' : 'client',
                'assignee_id' => $this->tagAssigneeId($request),
            ],
        ]);
    }

    public function detachTagFromChat(ChatTagRequest $request): void
    {
        try {
            $this->module->getApiClient()->delete('tags/:id/delete_from', [
                'params' => [ 'id' => $this->tagId($request->tag) ],
                'json' => [
                    ($request->forTicket ? 'request_id' : 'client_id') => $this->tagAssigneeId($request),
                ],
            ]);
        }

        catch (ValidationException $e) {
            if (!$e->notFound('assignee_id')) throw $e;
        }
    }

    private function tagId($tag): int
    {
        return (int)($tag instanceof Tag ? $tag->getExternalId() : $tag);
    }

    /**
     * @param \BmPlatform\Abstraction\Requests\ChatTagRequest $request
     *
     * @return mixed
     */
    protected function tagAssigneeId(ChatTagRequest $request): mixed
    {
        return $request->chat->getExtraData()[$request->forTicket
            ? ExtraDataProps::REQUEST_ID
            : ExtraDataProps::CLIENT_ID];
    }
}