<?php

namespace BmPlatform\Chat2Desk\Commands;

use BmPlatform\Abstraction\DataTypes\InlineButton;
use BmPlatform\Abstraction\DataTypes\InlineUrlButton;
use BmPlatform\Abstraction\DataTypes\QuickReply;
use BmPlatform\Abstraction\Enums\ButtonColor;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Enums\QuickReplyType;
use BmPlatform\Abstraction\Exceptions\ErrorException;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Requests\BaseMessageRequest;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Chat2Desk\Exceptions\ValidationException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;
use function with;

trait MessageCommands
{
    public function sendSystemMessage(SendSystemMessageRequest $request): void
    {
        $this->sendMessage($request->chat, [
            'type' => 'system',
            'text' => $request->text,
        ]);
    }

    public function sendMediaMessage(SendMediaRequest $request): MessageSendResult
    {
        return $this->sendMessageToClient($request, [
            ...($request->caption ? [ 'text' => $request->caption ] : []),
            ...$this->mediaMessagePayload($request),
        ]);
    }

    public function sendTextMessage(SendTextMessageRequest $request): MessageSendResult
    {
        return $this->sendMessageToClient($request, [ 'text' => $request->text ]);
    }

    protected function inlineButtonPayload(InlineButton $button): array
    {
        return match (true) {
            $button instanceof InlineUrlButton => [ 'url' => $button->url ],
            $button instanceof InlineButton => [ 'payload' => $button->payload ],
            default => [],
        };
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function sendMessage(Chat $chat, array $payload): MessageSendResult
    {
        try {
            return with($this->module->getApiClient()->post('messages', [
                'json' => $payload + [
                    'client_id' => $chat->getExtraData()[ExtraDataProps::CLIENT_ID],
                    ...Utils::channelTransportParams($chat->getMessengerInstance()),
                    'open_dialog' => false,
                ]
            ]), fn($resp) => new MessageSendResult($resp['message_id']));
        }

        catch (ValidationException $e) {
            throw match (true) {
                $e->notFound('client_id') => new ErrorException(ErrorCode::CustomerNotFound),
                $e->notFound('channel_id') || $e->notFound('transport') => new ErrorException(ErrorCode::InvalidMessengerInstance),
                $e->chatNotExists() => new ErrorException(ErrorCode::ChatNotFound),
                default => $e,
            };
        }
    }

    /** @param \BmPlatform\Abstraction\DataTypes\QuickReply[][] $buttons */
    protected function keyboardPayload(array $buttons): array
    {
        return with(collect($buttons)->flatten()->map(fn (QuickReply $quickReply) => [
            'type' => $this->convertQuickReplyType($quickReply->type),
            'text' => $quickReply->text,
            ...$this->buttonColorPayload($quickReply->color),
        ])->all(), function ($buttons) {
            return $buttons ? [ 'keyboard' => compact('buttons') ] : [];
        });
    }

    protected function convertQuickReplyType(QuickReplyType $type): string
    {
        return match ($type) {
            QuickReplyType::Location => 'location',
            QuickReplyType::Phone => 'phone',
            QuickReplyType::Text => 'reply',
        };
    }

    /**
     * @param \BmPlatform\Abstraction\Requests\SendMediaRequest $request
     *
     * @return array
     */
    protected function mediaMessagePayload(SendMediaRequest $request): array
    {
        return [
            'attachment' => $request->file->url,
            'attachment_filename' => $request->file->name,
        ];
    }

    protected function buttonColorPayload(?ButtonColor $color): array
    {
        return match ($color) {
            ButtonColor::Negative => [ 'color' => 'red' ],
            ButtonColor::Positive => [ 'color' => 'green' ],
            ButtonColor::Primary => [ 'color' => 'blue' ],
            default => [],
        };
    }

    protected function sendMessageToClient(BaseMessageRequest $request, array $payload): MessageSendResult
    {
        return $this->sendMessage($request->chat, $payload + [
                'type' => 'autoreply',
                ...$this->inlineButtonsPayload($request->inlineButtons),
                ...$this->keyboardPayload($request->quickReplies),
            ]);
    }

    /** @param \BmPlatform\Abstraction\DataTypes\InlineButton[][] $buttons */
    protected function inlineButtonsPayload(array $buttons): array
    {
        return with(collect($buttons)->flatten()->map(fn (InlineButton $button) => [
            'type' => $button instanceof InlineUrlButton ? 'url' : 'reply',
            'text' => $button->text,
            ...$this->inlineButtonPayload($button),
            ...$this->buttonColorPayload($button->color),
        ])->all(), function ($buttons) {
            return $buttons ? [ 'inline_buttons' => $buttons ] : [];
        });
    }
}