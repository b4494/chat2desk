<?php

namespace BmPlatform\Chat2Desk\Commands;

use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Interfaces\Operator;
use BmPlatform\Abstraction\Interfaces\OperatorGroup;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;

trait OperatorCommands
{
    public function transferChatToOperator(TransferChatToOperatorRequest $request): NewOperatorResponse
    {
        $messages = $this->module->getApiCommands()->messages([
            'client_id' => $request->chat->getExtraData()[ExtraDataProps::CLIENT_ID],
            ...Utils::channelTransportParams($request->chat->getMessengerInstance()),
            'limit' => 1,
            'order' => 'desc',
        ]);

        if ($request->target instanceof OperatorGroup) {
            $newOperatorId = $this->transferToGroup($messages[0]['id'], $request->target);
        } else {
            $newOperatorId = $this->transferToOperator($messages[0]['id'], $request->target);
        }

        return new NewOperatorResponse((string)$newOperatorId);
    }

    private function transferToGroup($messageId, OperatorGroup $target): int
    {
        try {
            $response = $this->module->getApiClient()->get('messages/:id/transfer_to_group', [
                'params' => [ 'id' => $messageId ],
                'query' => [ 'group_id' => $target->getExternalId() ],
            ]);

            return $response['assignee']['id'];
        }

        catch (ErrorException $e) {
            throw match (true) {
                $e->operatorGroupNotFound() => new ErrorException(ErrorCode::OperatorNotFound),
                default => $e,
            };
        }
    }

    private function transferToOperator($messageId, Operator $operator): int
    {
        try {
            $this->module->getApiClient()->get('messages/:id/transfer', [
                'params' => [ 'id' => $messageId ],
                'query' => [ 'operator_id' => $operator->getExternalId() ],
            ]);

            return $operator->getExternalId();
        }

        catch (ErrorException $e) {
            throw match (true) {
                $e->operatorNotFound() => new ErrorException(ErrorCode::OperatorNotFound),
                default => $e,
            };
        }
    }
}