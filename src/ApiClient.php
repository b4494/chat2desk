<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Chat2Desk\Exceptions\ErrorException;
use BmPlatform\Chat2Desk\Exceptions\ValidationException;
use BmPlatform\Support\Http\HttpClient;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\MessageBag;
use Psr\Http\Message\ResponseInterface;

class ApiClient extends HttpClient
{
    public function __construct(public string $domain, public string $token, array $options = [])
    {
        parent::__construct($options);
    }

    public function all(string $path, ?int $cursor, int $limit = 200): IterableData
    {
        $options['query']['limit'] = $limit;
        $options['query']['offset'] = $cursor = $cursor ?: 0;

        $response = $this->get($path, $options);

        return new IterableData($response, count($response) == $limit ? $cursor + $limit : null);
    }

    protected function defaultConfig(): array
    {
        return [
            'base_uri' => $this->domain.'/v1/',
            RequestOptions::HEADERS => [
                'Authorization' => $this->token,
                'Accept' => 'application/json',
                'X-BotMarketing' => 'HWsXBdbPw2hu2pKOVQCfWjQ0pPiOK1jV',
            ],
            RequestOptions::CONNECT_TIMEOUT => 3,
            RequestOptions::TIMEOUT => 60,
        ];
    }

    /** @throws \BmPlatform\Abstraction\Exceptions\ErrorException */
    protected function processResponse(ResponseInterface $response): mixed
    {
        if (500 <= $code = $response->getStatusCode()) {
            throw $this->handleServerError($code);
        }

        try {
            if ($data = $response->getBody()->getContents()) {
                return $this->parseData($data, $code);
            } else {
                return $this->handleEmptyResponse($code);
            }
        }

        finally {
            $response->getBody()->close();
        }
    }

    private function handleServerError(int $code): ErrorException
    {
        return match ($code) {
            502 => new ErrorException(ErrorCode::ServiceUnavailable),
            default => new ErrorException(ErrorCode::InternalServerError, $code),
        };
    }

    private function handleEmptyResponse(int $code): bool
    {
        return $code >= 400 ? throw new ErrorException(ErrorCode::UnexpectedServerError, $code) : true;
    }

    private function parseData(string $data, int $code)
    {
        try {
            $data = json_decode($data, true, 512, JSON_INVALID_UTF8_IGNORE | JSON_THROW_ON_ERROR);
        }

        catch (\JsonException) {
            throw new ErrorException(ErrorCode::InvalidResponseData, 'Failed to parse json');
        }

        if (!isset($data['status']) && !isset($data['data']) && $code < 300) {
            return $data ?: true;
        }

        if (in_array(($status = $data['status'] ?? null), [ 'success', 'ok' ])) {
            return $data['data'] ?? true;
        }

        $message = $data['message'] ?? null;
        $errors = $data['errors'] ?? null;

        throw match (true) {
            is_array($errors) => new ValidationException(new MessageBag((array)$data['errors'])),
            $message == 'forbidden' && $errors == 'Token is not correct' => new ErrorException(ErrorCode::AuthenticationFailed),
            $code == 401 && $errors == 'Company is turned off' => new ErrorException(ErrorCode::AccountDisabled),
            $code == 429 => new ErrorException(ErrorCode::TooManyRequests),
            $code == 404 || $message == 'not_found' => new ErrorException(ErrorCode::NotFound, $errors),
            default => new ErrorException(ErrorCode::UnexpectedServerError, $errors ?: $message ?: $status ?: 'Unknown error'),
        };
    }
}