<?php

namespace BmPlatform\Chat2Desk;

use JetBrains\PhpStorm\ArrayShape;

class ApiCommands
{
    public function __construct(public readonly ApiClient $apiClient)
    {
        //
    }

    #[ArrayShape([
        'id' => 'int',
        'phone' => 'string',
        'comment' => 'string',
        'custom_fields' => 'array',
        'channels' => [ 'id' => 'int', 'transports' => 'array' ],
        'tags' => [['id' => 'int', 'label' => 'string']]
    ])]
    public function client($id): array
    {
        return $this->apiClient->get('clients/:id', [ 'params' => compact('id') ]);
    }

    /**
     * @param array{
     *     client_id: int,
     *     channel_id: int,
     *     transport: string,
     *     order: string,
     *     limit: int,
     *     dialog_id: int,
     * } $filter
     *
     * @return array
     */
    public function messages(array $filter = []): array
    {
        return $this->apiClient->get('messages', [ 'query' => $filter ]);
    }

    public function message($id): array
    {
        return $this->apiClient->get('messages/:id', [ 'params' => compact('id') ]);
    }

    #[ArrayShape([
        'id' => 'int',
        'email' => 'string',
        'phone' => 'string',
        'online' => 'int',
        'offline_type' => 'string',
        'avatar' => 'string',
        'first_name' => 'string',
        'last_name' => 'string',
        'last_visit' => 'string',
        'external_id' => 'string',
        'opened_dialogs' => 'int',
        'status_id' => 'int',
    ])]
    public function operatorByEmail($email): array
    {
        return collect($this->apiClient->get('operators', [ 'query' => compact('email') ]))->first();
    }

    #[ArrayShape([
        'id' => 'int',
        'dialog_id' => 'int',
        'tags' => 'array',
    ])]
    public function request($id): array
    {
        return $this->apiClient->get('requests/:requestId', [
            'params' => [ 'requestId' => $id ],
        ]);
    }

    /** @return array<array-key, array{id: int, operator_id: int, state: string, begin: string, end: string}> */
    public function clientDialogs($id): array
    {
        return $this->apiClient->get('clients/:id/dialogs', [
            'params' => compact('id'),
        ]);
    }
}