<?php

namespace BmPlatform\Chat2Desk;

use BmPlatform\Chat2Desk\Utils\DataWrap;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;

class EventHandler
{
    public function __construct()
    {
        //
    }

    public function __invoke(DataWrap $data)
    {
        $class = 'BmPlatform\Chat2Desk\EventHandlers\\'.Str::studly($data['hook_type']);

        if (!class_exists($class)) {
            return null;
        }

        if (!$result = (new $class)($data)) return null;

        if (!$result instanceof \Traversable && !is_array($result)) {
            $result = [ $result ];
        }

        foreach ($result as $event) {
            if ($event) $data->module->user->dispatchEvent($event);
        }
    }
}