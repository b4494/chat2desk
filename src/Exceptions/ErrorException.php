<?php

namespace BmPlatform\Chat2Desk\Exceptions;

use BmPlatform\Abstraction\Enums\ErrorCode;
use Illuminate\Support\Str;

class ErrorException extends \BmPlatform\Abstraction\Exceptions\ErrorException
{
    public function operatorGroupNotFound(): bool
    {
        return $this->errorCode === ErrorCode::NotFound && Str::contains($this->getMessage(), 'Operator Group');
    }

    public function operatorNotFound(): bool
    {
        return $this->errorCode === ErrorCode::NotFound && Str::contains($this->getMessage(), 'Operator');
    }
}