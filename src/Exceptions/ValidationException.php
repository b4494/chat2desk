<?php

namespace BmPlatform\Chat2Desk\Exceptions;

use Illuminate\Support\Str;

class ValidationException extends \BmPlatform\Abstraction\Exceptions\ValidationException
{
    public function notFound($param): bool
    {
        if (!$this->errors->has($param)) return false;

        foreach ($this->errors->get($param) as $msg) {
            if (Str::contains($msg, [ 'not found' ], ignoreCase: true)) {
                return true;
            }
        }

        return false;
    }

    public function chatNotExists()
    {
        return Str::contains($this->errors->first('client_id'), 'does not exist with specified transport or channel');
    }
}