<?php

use Illuminate\Support\Facades\Route;

Route::namespace('BmPlatform\Chat2Desk\Http')->name('c2d.')->group(function () {
    Route::post('integrations/c2d/webhook/{callbackId}', 'CallbackController')->name('callback');
    Route::get('integrations/c2d/{apiToken}', 'IntegrationController')->name('integration');
});

