<?php

namespace BmPlatform\Chat2Desk\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Chat2Desk\EventHandler;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use Illuminate\Http\Request;

class CallbackController
{
    public function __invoke(Request $request, Hub $hub, $callbackId)
    {
        $app = $hub->findAppByCallbackId('c2d', $callbackId);
        $data = new DataWrap($app->getHandler(), $request->toArray());

        logger('WH', [
            'appInstance' => $app,
            'body' => $data->data,
        ]);

        try {
            (new EventHandler)($data);
        } catch (\Throwable $e) {
            logger()->warning('Failed to process webhook', [
                'body' => $e,
                'appInstance' => $app,
            ]);
        }
    }
}