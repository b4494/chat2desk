<?php

namespace BmPlatform\Chat2Desk\Http;

use BmPlatform\Abstraction\Interfaces\Hub;
use BmPlatform\Chat2Desk\BillingCommands;
use BmPlatform\Chat2Desk\Integrator;
use BmPlatform\Chat2Desk\SetWebhook;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Lazychaser\CdBilling\CompanyApi;

class IntegrationController
{
    public function __invoke(Request $request, Hub $hub, $apiToken, BillingCommands $billingStatusResolver)
    {
        $forCatalog = env('C2D_FOR_CATALOG', $request->query('for_catalog', false));

        // Make an app data that contains all info to create user on the other side
        $makeAppData = new Integrator($request->query() + [ 'token' => $apiToken ], $billingStatusResolver, $forCatalog);

        // Make or update app instance from app data
        $instance = $hub->integrate($makeAppData());

        // Redirect user to the web interface
        return new RedirectResponse($hub->webUrl($hub->getAuthToken($instance)));
    }
}