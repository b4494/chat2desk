<?php

namespace BmPlatform\Chat2Desk\Processors;

use BmPlatform\Chat2Desk\AppHandler;
use BmPlatform\Chat2Desk\Utils\ExtraDataProps;
use BmPlatform\Chat2Desk\Utils\Utils;

class AppendCustomFields
{
    public function __invoke(AppHandler $handler, array $properties): array
    {
        $uid = $handler->user->getExternalId();

        foreach ($handler->user->getExtraData()[ExtraDataProps::CUSTOM_FIELDS] ?? [] as $customField) {
            [$name, $variableRef] = Utils::customFieldVar($customField);

            $properties['customField'.$customField['id']] = [
                'type' => $customField['type'] == 'input' ? 'text' : 'enum',
                'variableRef' => $variableRef,
                'isCustom' => true,
                'title' => $name,
                ...($customField['type'] == 'options' ? [
                    'options' => array_map(fn ($v) => [ 'value' => $v ], $customField['options']),
                    'withLabels' => false,
                ] : []),
            ];
        }

        return $properties;
    }
}