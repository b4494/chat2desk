<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\Events\OutboxSent;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\EventHandlers\Outbox;
use BmPlatform\Chat2Desk\AppHandler;
use Carbon\Carbon;
use Mockery as m;

class OutboxTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testOutbox()
    {
        $dataText = '	{
  "message_id": 1561540797,
  "type": "to_client",
  "text": "«79511431221»\n— Привет",
  "transport": "telegram",
  "client_id": 10379672,
  "operator_id": 26932,
  "dialog_id": 2358799,
  "channel_id": 2811,
  "photo": null,
  "coordinates": null,
  "audio": null,
  "pdf": null,
  "client": {
    "id": 10379672,
    "phone": "[tg] 192107759",
    "client_phone": "79511431221",
    "name": "Alexander Kalnoy",
    "assigned_name": "Кальной Александр",
    "external_id": null
  },
  "hook_type": "outbox",
  "request_id": 193091012,
  "attachments": [],
  "is_new_request": false,
  "is_new_client": false,
  "insta_comment": false,
  "extra_data": [],
  "is_new": false,
  "event_time": "2022-03-21T09:07:10Z"
}';

        $data = m::mock(DataWrap::class, [ m::mock(AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]), json_decode($dataText, true)]);
        $data->expects('chat')->andReturn($chat = m::mock(Chat::class));
        $data->expects('timestamp')->andReturn($ts = new Carbon());
        $data->makePartial();

        $outbox = new Outbox();

        $this->assertEquals(new OutboxSent(
            chat: $chat,
            message: new MessageData(
                externalId: '1561540797',
                text: 'Привет',
                extraData: ['quote'=>'79511431221'],
            ),
            operator: '26932',
            timestamp: $ts,
        ), $outbox($data));
    }
}