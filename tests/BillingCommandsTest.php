<?php

use BmPlatform\Chat2Desk\BillingCommands;
use Mockery as m;

class BillingCommandsTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testHasActiveServices(): void
    {
        $resolver = new BillingCommands($api = m::mock(\Lazychaser\CdBilling\CompanyApi::class), [
            'serviceIds' => [ 123 ],
        ]);

        $api->expects('activeServices')->with('billingId')->andReturn(new \Lazychaser\CdBilling\DataTypes\ActiveServiceCollection([
            'services' => [
                [ 'serviceTypeId' => 123, 'status' => 'active' ],
            ],
            'currency' => 643,
        ]));
        $result = $resolver->resolveCompanyStatus('billingId');
        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\AppExternalStatus(
            \BmPlatform\Abstraction\Enums\AppStatus::Active,
            \BmPlatform\Abstraction\Enums\PaymentStatus::Paid,
            externalTariffId: 123,
        ), $result);

        $api->expects('activeServices')->with('billingId')->andReturn(new \Lazychaser\CdBilling\DataTypes\ActiveServiceCollection([ 'currency' => 643, 'services' => []]));
        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\AppExternalStatus(
            \BmPlatform\Abstraction\Enums\AppStatus::Active,
        ), $resolver->resolveCompanyStatus('billingId'));

        $api->expects('activeServices')->with('billingId')->andReturn(new \Lazychaser\CdBilling\DataTypes\ActiveServiceCollection([ 'currency' => 643, 'services' => [
            [ 'serviceTypeId' => 123, 'status' => 'suspended' ],
        ]]));
        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\AppExternalStatus(
            \BmPlatform\Abstraction\Enums\AppStatus::Active,
            \BmPlatform\Abstraction\Enums\PaymentStatus::NotPaid,
            externalTariffId: 123,
        ), $resolver->resolveCompanyStatus('billingId'));

        $api->expects('activeServices')->with('billingId')->andThrow(new \Lazychaser\CdBilling\BillingClientException('test'));
        $this->assertNull($resolver->resolveCompanyStatus('billingId'));
    }

    public function testGetPaymentForServices(): void
    {
        $commands = new BillingCommands($api = m::mock(\Lazychaser\CdBilling\CompanyApi::class), [
            'defaultOneTimeServiceId' => 123,
        ]);

        $api->expects('activeServices')->with('companyId')->andReturn(new \Lazychaser\CdBilling\DataTypes\ActiveServiceCollection(
            [ 'currency' => 643, 'services' => []]
        ));

        $api->expects('activateService')->with('companyId', [
            'serviceTypeId' => 123,
            'serviceInstanceId' => 12345678,
            'quantity' => 100,
            'serviceMode' => \Lazychaser\CdBilling\DataTypes\ServiceMode::standard,
            'eventDate' => 12345678,
        ]);

        $api->expects('balance')->with('companyId')->andReturn(new \Lazychaser\CdBilling\DataTypes\BalanceInfo([
            'currentBalance' => 100,
            'thresholdTurnoffValue' => 0,
        ]));

        $commands->getPaymentForServices('companyId', [
            new \BmPlatform\Abstraction\DataTypes\ReceiptItem(1, null, 'test', \Brick\Money\Money::of(50, 'RUB'), 1),
            new \BmPlatform\Abstraction\DataTypes\ReceiptItem(2, null, 'test 2', \Brick\Money\Money::of(25, 'RUB'), 2),
        ], 12345678);
    }
}