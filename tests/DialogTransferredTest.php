<?php

use BmPlatform\Chat2Desk\Utils\DataWrap;
use Mockery as m;

class DialogTransferredTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testDialogTransferredEvent()
    {
        $dataJson = '{
  "dialog_id": 48207525,
  "request_id": null,
  "created": 1647862665,
  "updated": 1647862665,
  "channel_id": 63436,
  "client_id": 24313239,
  "operator_id": 243951,
  "current_operator_id": 243951,
  "transport": "telegram",
  "last_operator_id": null,
  "scenario_id": null,
  "hook_type": "dialog_transferred",
  "event_time": "2022-03-21T11:37:45Z"
}';

        $data = m::mock(DataWrap::class, [ m::mock(\BmPlatform\Chat2Desk\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]), json_decode($dataJson, true)]);
        $data->expects('chat')->andReturn($chat = m::mock(\BmPlatform\Abstraction\DataTypes\Chat::class));
        $data->expects('timestamp')->andReturn($ts = Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new \BmPlatform\Chat2Desk\EventHandlers\DialogTransferred();

        $this->assertEquals(new \BmPlatform\Abstraction\Events\ChatTransferredToOperator(
            chat: $chat,
            newOperator: '243951',
            timestamp: $ts,
        ), $eventHandler($data));
    }
}