<?php

use BmPlatform\Abstraction\DataTypes\IterableData;
use BmPlatform\Abstraction\DataTypes\Operator;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Chat2Desk\ApiClient;
use BmPlatform\Chat2Desk\AppHandler;
use Illuminate\Contracts\Config\Repository;

class ModuleTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testCreatesApiClient()
    {
        $user = Mockery::mock(AppInstance::class);
        $user->expects('getExternalId')->andReturn('ru:123');
        $user->expects('getExtraData')->andReturn([ 'token' => 'token' ]);

        $config = Mockery::mock(Repository::class);
        $config->expects('get')->with('c2d.servers.ru.api_domain')->andReturn('https://api.domain.com');

        $module = new AppHandler($user, $config, Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class));
        $apiClient = $module->getApiClient();

        $this->assertEquals('https://api.domain.com', $apiClient->domain);
        $this->assertEquals('token', $apiClient->token);
    }

    public function testMessengerInstances()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('get')->with('channels')->andReturn([
            ['id' => 1, 'name' => 'test', 'transports'=>['telegram', 'instagram']],
            ['id' => 2, 'name' => 'test 2', 'transports'=>['whatsapp', 'invalid']],
        ]);

        $user = Mockery::mock(AppInstance::class);
        $config = Mockery::mock(Repository::class);

        $config->expects('get')->with('c2d.messenger_types')->zeroOrMoreTimes()->andReturn([ 'telegram' => 1, 'instagram' => 1, 'whatsapp' => 1]);
        $config->expects('get')->with('c2d.messenger_types.telegram.internal_type')->zeroOrMoreTimes()->andReturn('telegram');
        $config->expects('get')->with('c2d.messenger_types.instagram.internal_type')->zeroOrMoreTimes()->andReturn('instagram');
        $config->expects('get')->with('c2d.messenger_types.whatsapp.internal_type')->zeroOrMoreTimes()->andReturn('whatsapp');
        $config->expects('get')->with('c2d.messenger_types.telegram.name')->zeroOrMoreTimes()->andReturn('telegram');
        $config->expects('get')->with('c2d.messenger_types.instagram.name')->zeroOrMoreTimes()->andReturn('instagram');
        $config->expects('get')->with('c2d.messenger_types.whatsapp.name')->zeroOrMoreTimes()->andReturn('whatsapp');

        $module = new AppHandler($user, $config, Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class), null, null, $apiClient);
        $result = $module->getMessengerInstances();

        $this->assertCount(3, $result->data);
        $this->assertInstanceOf(\BmPlatform\Abstraction\DataTypes\MessengerInstance::class, $result->data[0]);
        $this->assertEquals('telegram', $result->data[0]->externalType);
        $this->assertEquals('1:telegram', $result->data[0]->externalId);
        $this->assertEquals('test telegram', $result->data[0]->name);
        $this->assertEquals(\BmPlatform\Abstraction\Enums\MessengerType::Telegram, $result->data[0]->internalType);

        $this->assertEquals('test 2', $result->data[2]->name);
    }

    public function testGetsOperators()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('all')->with('operators', null)->andReturn(new IterableData([
            [ 'id' => 1, 'role' => 'admin', 'first_name' => 'Администратор', 'email' => 'admin@mail.ru', 'last_name' => '', 'avatar' => null ],
            [ 'id' => 2, 'role' => 'operator', 'first_name' => 'Иван', 'email' => 'operator@mail.ru', 'last_name' => 'Иванов', 'avatar' => 'avatar' ],
            [ 'id' => 3, 'role' => 'disabled', 'first_name' => 'Сидр', 'email' => 'dusabked@mail.ru', 'last_name' => 'Сидоров', 'avatar' => null ],
        ], null));

        $user = Mockery::mock(AppInstance::class);
        $config = Mockery::mock(Repository::class);
        $module = new AppHandler($user, $config, Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class), null, null, $apiClient);
        $result = $module->getOperators();

        $this->assertCount(2, $result->data);
        $this->assertNull($result->cursor);
        $this->assertEquals(new Operator(1, name: 'Администратор', email: 'admin@mail.ru'), $result->data[0]);
        $this->assertEquals(new Operator(2, name: 'Иван Иванов', email: 'operator@mail.ru', avatarUrl: 'avatar'), $result->data[1]);
    }

    public function testGetsTags()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('all')->with('tags', null)->andReturn(new IterableData(json_decode('[{
            "id": 68319,
            "group_id": 16311,
            "group_name": "Admins tags",
            "label": "MARK",
            "description": "Marker"
        },
        {
            "id": 73022,
            "group_id": 16311,
            "group_name": "Admins tags",
            "label": "RESEND",
            "description": "Resend new"
        }]', true), null));

        $user = Mockery::mock(AppInstance::class);
        $config = Mockery::mock(Repository::class);
        $module = new AppHandler($user, $config, Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class), null, null, $apiClient);

        $result = $module->getTags();
        $this->assertCount(2, $result->data);
        $this->assertNull($result->cursor);
        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\Tag('68319', 'MARK', 'Admins tags'), $result->data[0]);
    }

    public function generatorFromArray(array $array)
    {
        foreach ($array as $i) {
            yield $i;
        }
    }

    public function testGetPaymentForResources(): void
    {
        $billingCommands = Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class);
        $handler = new AppHandler($app = Mockery::mock(AppInstance::class), Mockery::mock(Repository::class), $billingCommands);

        $app->expects('getExtraData')->andReturn([ 'billingId' => 'companyId' ]);

        $items = [];
        $billingCommands->expects('getPaymentForServices')->with('companyId', $items);

        $handler->getPaymentForServices($items);
    }
}