<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\Contact;
use BmPlatform\Abstraction\DataTypes\GeoLocation;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\MessageData;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Enums\InboxFlags;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Events\InboxReceived;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\EventHandler;
use BmPlatform\Chat2Desk\EventHandlers\Inbox;
use BmPlatform\Chat2Desk\AppHandler;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class InboxHandlerTest extends TestCase
{
    /** @dataProvider dataProvider */
    public function testInboxEvents($data, $event)
    {
        $data = m::mock(DataWrap::class, [
            m::mock(AppHandler::class, [
                $u = $this->mockAppInstance(),
                m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
            ]), json_decode($data, true),
        ]);
        $data->expects('chat')->andReturn($this->mockChat());
        $data->expects('timestamp')->andReturn($this->mockTimestamp());
        $data->makePartial();

        $eventHandler = new Inbox();

        $this->assertEquals($event, $eventHandler($data));
    }

    public function dataProvider(): array
    {
        return [
            'basic inbox' => [
                '{
  "message_id": 1558118395,
  "client_id": 10379672,
  "request_id": 195233854,
  "type": "from_client",
  "client": {
    "id": 10379672,
    "phone": "79205550123",
    "client_phone": null,
    "name": "Alexander Kalnoy",
    "assigned_name": null,
    "external_id": null
  },
  "text": "ИИ",
  "photo": null,
  "coordinates": null,
  "audio": null,
  "pdf": null,
  "hook_type": "inbox",
  "attachments": [],
  "is_new_request": false,
  "is_new_client": false,
  "insta_comment": false,
  "extra_data": [],
  "is_new": true,
  "event_time": "2022-03-18T20:43:47Z"
}',
                new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: '10379672',
                        name: 'Alexander Kalnoy',
                        phone: '79205550123',
                        extraData: [ 'clph' => '79205550123' ],
                    ),
                    message: new MessageData(
                        externalId: '1558118395',
                        text: 'ИИ',
                    ),
                    timestamp: $this->mockTimestamp(),
                ),
            ],

            'photo' => [
                '{
  "message_id": 1557349251,
  "client_id": 10379672,
  "request_id": 195233854,
  "type": "from_client",
  "client": {
    "id": 10379672,
    "phone": "79205550123",
    "client_phone": null,
    "name": "Alexander Kalnoy",
    "assigned_name": null,
    "external_id": null
  },
  "text": null,
  "photo": "url",
  "coordinates": null,
  "audio": null,
  "pdf": null,
  "hook_type": "inbox",
  "attachments": [
    {
      "id": 114828850,
      "resource_type": "Message",
      "resource_id": 1557349251,
      "file": {
        "url": "url"
      },
      "hard_link": "/companies/company_22557/messages/2022-3",
      "content_type": "image/jpeg",
      "created_at": "2022-03-18T12:32:12.000Z",
      "updated_at": "2022-03-18T12:32:12.000Z",
      "company_id": 22557,
      "original_file_name": null,
      "file_size": 126622
    }
  ],
  "is_new_request": true,
  "is_new_client": true,
  "insta_comment": false,
  "extra_data": [],
  "is_new": true,
  "event_time": "2022-03-18T12:32:12Z"
}', new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: '10379672',
                        name: 'Alexander Kalnoy',
                        phone: '79205550123',
                        extraData: [ 'clph' => '79205550123' ],
                    ),
                    message: new MessageData(
                        externalId: '1557349251',
                        attachments: [ new MediaFile(MediaFileType::Image, 'url', 'image/jpeg') ],
                    ),
                    flags: InboxFlags::NEW_CHAT_CREATED | InboxFlags::NEW_TICKET_OPENED,
                    timestamp: $this->mockTimestamp(),
                ),
            ],
            'coordinates' => [
                '{
  "message_id": 1561498835,
  "type": "from_client",
  "text": null,
  "transport": "telegram",
  "client_id": 10379672,
  "operator_id": null,
  "dialog_id": 2358799,
  "channel_id": 2811,
  "photo": null,
  "coordinates": "38.088273 50.213633",
  "audio": null,
  "pdf": null,
  "client": {
    "id": 10379672,
    "phone": "[tg] 192107759",
    "client_phone": null,
    "name": "Alexander Kalnoy",
    "assigned_name": null,
    "external_id": null
  },
  "hook_type": "inbox",
  "request_id": 193091012,
  "attachments": [],
  "is_new_request": false,
  "is_new_client": false,
  "insta_comment": false,
  "extra_data": [],
  "is_new": true,
  "event_time": "2022-03-21T08:49:36Z"
}',
                new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: '10379672',
                        name: 'Alexander Kalnoy',
                        extraData: [ 'clph' => '[tg] 192107759' ],
                    ),
                    message: new MessageData(
                        externalId: '1561498835',
                        geoLocation: new GeoLocation('38.088273', '50.213633'),
                    ),
                    timestamp: $this->mockTimestamp(),
                ),
            ],
            'quoted' => [
                '	{
  "message_id": 1561508102,
  "type": "from_client",
  "text": "«Привет»\nпривет",
  "transport": "telegram",
  "client_id": 10379672,
  "operator_id": 26932,
  "dialog_id": 2358799,
  "channel_id": 2811,
  "photo": null,
  "coordinates": null,
  "audio": null,
  "pdf": null,
  "client": {
    "id": 10379672,
    "phone": "[tg] 192107759",
    "client_phone": "79205550123",
    "name": "Alexander Kalnoy",
    "assigned_name": "Кальной Александр",
    "external_id": null
  },
  "hook_type": "inbox",
  "request_id": 193091012,
  "attachments": [],
  "is_new_request": false,
  "is_new_client": false,
  "insta_comment": false,
  "extra_data": [],
  "is_new": false,
  "event_time": "2022-03-21T08:53:40Z"
}',
                new InboxReceived(
                    chat: $this->mockChat(),
                    participant: new Contact(
                        externalId: '10379672',
                        name: 'Кальной Александр',
                        phone: '79205550123',
                        extraData: [ 'clph' => '[tg] 192107759' ],
                    ),
                    message: new MessageData(
                        externalId: '1561508102',
                        text: 'привет',
                        extraData: [ 'quote' => 'Привет' ],
                    ),
                    timestamp: $this->mockTimestamp(),
                ),
            ],
        ];
    }

    public function mockAppInstance(): \BmPlatform\Abstraction\Interfaces\AppInstance
    {
        static $value;

        return isset($value) ? $value : $value = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class);
    }

    public function mockChat()
    {
        static $chat;

        if (isset($chat)) return $chat;

        return $chat = new Chat('chat', 'id');
    }

    public function mockTimestamp()
    {
        static $ts;

        if (isset($ts)) return $ts;

        return $ts = new Carbon();
    }
}