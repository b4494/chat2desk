<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Chat2Desk\ApiClient;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use BmPlatform\Chat2Desk\EventHandler;
use BmPlatform\Chat2Desk\AppHandler;
use Mockery as m;

class DataWrapTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testMessengerInstance()
    {
        $data = new DataWrap(Mockery::mock(AppHandler::class), [
            'channel_id' => 1,
            'transport' => 'transport',
        ]);

        $this->assertEquals('1:transport', $data->messengerInstance());
    }

    public function testChatWithDialogId()
    {
        $data = new DataWrap(m::mock(AppHandler::class), [
            'dialog_id' => 1,
            'request_id' => 2,
            'operator_id' => 3,
            'client_id' => 4,
            'transport' => 'transport',
            'channel_id' => 5,
            'client' => [
                'phone' => '[tg] 123',
            ],
        ]);

        $this->assertEquals(new Chat('1',
            '5:transport',
            contact: '4',
            operator: '3',
            messengerId: '123',
            extraData: [
                'clid' => 4,
                'reqid' => 2,
            ],
        ), $data->chat());
    }

    public function testDialogIdWithoutDialogId()
    {
        $apiCommands = m::mock(\BmPlatform\Chat2Desk\ApiCommands::class);
        $apiCommands->expects('request')->with(1)->andReturn([ 'dialog_id' => 2 ]);

        $module = m::mock(AppHandler::class);
        $module->expects('getApiCommands')->andReturn($apiCommands);

        $dataWrap = new DataWrap($module, [
            'dialog_id' => null,
            'request_id' => 1,
        ]);

        $this->assertEquals(2, $dataWrap->dialogId());
    }
}