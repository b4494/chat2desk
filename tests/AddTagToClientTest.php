<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Chat2Desk\EventHandlers\AddTagToClient;
use BmPlatform\Chat2Desk\AppHandler;
use Mockery as m;

class AddTagToClientTest extends m\Adapter\Phpunit\MockeryTestCase
{
    public static function setUpBeforeClass(): void
    {
        \Illuminate\Support\Facades\Cache::swap(new \Illuminate\Cache\Repository(new \Illuminate\Cache\NullStore()));
    }

    public function testAddTagToClientEvent()
    {
        $apiCommands = m::mock(\BmPlatform\Chat2Desk\ApiCommands::class);

        $apiCommands->expects('clientDialogs')->with(1)->andReturn([
            [ 'id' => 10 ],
        ]);

        $module = m::mock(
            AppHandler::class,
            [ $u = m::mock(AppInstance::class), m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class) ]
        );
        $module->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();

        $data = m::mock(\BmPlatform\Chat2Desk\Utils\DataWrap::class, [
            $module, [
                'client_id' => 1,
                'id' => 100,
                'name' => 'label',
                'group_name' => 'group',
                'channel_id' => 1000,
                'transport' => 'telegram',
            ],
        ]);
        $data->expects('timestamp')->andReturn($ts = \Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new AddTagToClient();

        $this->assertEquals([
            new AttachedTagToChat(
                chat: new Chat(
                    externalId: '10',
                    messengerInstance: '1000:telegram',
                    contact: '1',
                    extraData: [ 'clid' => 1 ],
                ),
                tag: new \BmPlatform\Abstraction\DataTypes\Tag(100, 'label', 'group'),
                timestamp: $ts
            )],
            $eventHandler($data),
        );
    }

    public function testAddTagWithSeveralDialogs(): void
    {
        $apiCommands = m::mock(\BmPlatform\Chat2Desk\ApiCommands::class);

        $apiCommands->expects('clientDialogs')->with(1)->andReturn([
            [ 'id' => 10 ], [ 'id' => 20 ],
        ]);

        $apiCommands->expects('messages')->with([
            'client_id' => 1,
            'dialog_id' => 10,
            'limit' => 1,
            'order' => 'desc',
        ])->andReturn([
            [ 'id' => 10, 'client_id' => 1, 'dialog_id' => 10, 'transport' => 'telegram', 'channel_id' => 1000 ],
        ]);

        $apiCommands->expects('messages')->with([
            'client_id' => 1,
            'dialog_id' => 20,
            'limit' => 1,
            'order' => 'desc',
        ])->andReturn([
            [ 'id' => 11, 'client_id' => 1, 'dialog_id' => 20, 'transport' => 'telegram', 'channel_id' => 2000 ],
        ]);

        $module = m::mock(
            AppHandler::class,
            [ $u = m::mock(AppInstance::class), m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class) ]
        );
        $module->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();

        $data = m::mock(\BmPlatform\Chat2Desk\Utils\DataWrap::class, [
            $module, [
                'client_id' => 1,
                'id' => 100,
                'name' => 'label',
                'group_name' => 'group',
                'channel_id' => 1000,
                'transport' => 'telegram',
            ],
        ]);
        $data->expects('timestamp')->times(2)->andReturn($ts = \Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new AddTagToClient();

        $tag = new \BmPlatform\Abstraction\DataTypes\Tag(100, 'label', 'group');
        $this->assertEquals([
            new AttachedTagToChat(
                chat: new Chat(
                    externalId: '10',
                    messengerInstance: '1000:telegram',
                    contact: '1',
                    extraData: [ 'clid' => 1 ],
                ),
                tag: $tag,
                timestamp: $ts
            ), new AttachedTagToChat(
                chat: new Chat(
                    externalId: '20',
                    messengerInstance: '2000:telegram',
                    contact: '1',
                    extraData: [ 'clid' => 1 ],
                ),
                tag: $tag,
                timestamp: $ts
            )],
            $eventHandler($data),
        );
    }
}