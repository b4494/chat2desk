<?php
use BmPlatform\Chat2Desk\AppHandler;
use BmPlatform\Chat2Desk\Utils\DataWrap;
use Mockery as m;

class NewClientTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testNewClientEvent(): void
    {
        $json = <<<'EOT'
{
  "id": 31074670,
  "phone": "[tg] 7536765434",
  "name": "Nocolas",
  "avatar": "",
  "assigned_name": null,
  "comment": null,
  "region_id": null,
  "country_id": null,
  "extra_comment_1": null,
  "extra_comment_2": null,
  "extra_comment_3": null,
  "client_created": "2023-01-25T10:14:46 UTC",
  "client_updated": "2023-01-25T10:14:46 UTC",
  "channels": [
    {
      "id": 18333,
      "transports": [
        "wa_dialog"
      ]
    }
  ],
  "tags": [],
  "client_source": "write_first",
  "hook_type": "new_client",
  "event_time": "2023-01-25T10:14:46.699087Z"
}
EOT;

        $data = m::mock(DataWrap::class, [
            $handler = m::mock(AppHandler::class), json_decode($json, true),
        ]);
        $data->expects('timestamp')->andReturn($now = \Carbon\Carbon::now());
        $data->makePartial();

        $handler->expects('getApiCommands')->andReturn($client = m::mock(\BmPlatform\Chat2Desk\ApiCommands::class));

        $client->expects('clientDialogs')->with(31074670)->andReturn([['id'=>123,'operator_id'=>234]]);

        $eventHandler = new \BmPlatform\Chat2Desk\EventHandlers\NewClient();
        $result = $eventHandler($data);

        $this->assertInstanceOf(\BmPlatform\Abstraction\Events\NewContactChatAdded::class, $result);
        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\Chat('123', '18333:wa_dialog', '31074670', '234', '7536765434', ['clid'=>31074670]), $result->chat);
        $this->assertEquals([ \BmPlatform\Chat2Desk\Utils\ExtraDataProps::CLIENT_SOURCE => 'write_first' ], $result->extraData);
        $this->assertEquals($now, $result->timestamp);
    }
}