<?php

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class ApiClientTest extends \PHPUnit\Framework\TestCase
{
    protected function initClient(array $responses)
    {
        $handler = new \GuzzleHttp\Handler\MockHandler($responses);

        return new \BmPlatform\Chat2Desk\ApiClient('https://domain.com', 'token', [
            'handler' => new \GuzzleHttp\HandlerStack($handler),
        ]);
    }

    public function testRequests()
    {
        $apiClient = $this->initClient([
            function (Request $request) {
                if ((string)$request->getUri() == 'https://domain.com/v1/api_info'
                    && $request->getHeader('Authorization')[0] == 'token'
                ) {
                    return new Response(200, [], '');
                }

                return new Response(400, [], '');
            },
            new Response(200, [], json_encode([ 'data' => 'hello, world!', 'status' => 'ok'])),
            new Response(400, [], '{"message":"bad_request","errors":{"client_id":["error"]},"status":"error"}'),
            new Response(404, [], '{"message":"not_found","errors":"Message #354949562 not found","status":"error"}'),
            new Response(403, [], '{"message":"forbidden","errors":"Token is not correct","status":"error"}'),
            new Response(401, [], '{"message":"unauthorized","errors":"Company is turned off","status":"error"}'),
            new Response(200, [], '{"dialogId":1234}'),
        ]);

        $this->assertEquals(true, $apiClient->request('GET', 'api_info'));
        $this->assertEquals('hello, world!', $apiClient->request('GET', ''));

        try {
            $apiClient->get('');
            $this->fail('Expected validation exception');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ValidationException $e) {
            $this->assertEquals('error', $e->errors->first('client_id'));
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::NotFound, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::AuthenticationFailed, $e->errorCode);
        }

        try {
            $apiClient->get('');
            $this->fail('Expected error exception');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::AccountDisabled, $e->errorCode);
        }

        $resp = $apiClient->get('');
        $this->assertEquals(['dialogId' => 1234], $resp);
    }

    public function testAll()
    {
        $apiClient = Mockery::mock(\BmPlatform\Chat2Desk\ApiClient::class)->makePartial();

        $apiClient->expects('get')->with('operators', [ 'query' => [ 'offset' => 0, 'limit' => 200 ] ])->andReturn([]);
        $resp = $apiClient->all('operators', null);
        $this->assertNull($resp->cursor);
        $this->assertEquals([], $resp->data);

        $apiClient->expects('get')->with('operators', [ 'query' => [ 'offset' => 0, 'limit' => 1 ]])->andReturn([ 'test' ]);
        $resp = $apiClient->all('operators', null, 1);
        $this->assertCount(1, $resp->data);
        $this->assertEquals('test', $resp->data[0]);
        $this->assertEquals(1, $resp->cursor);
    }
}