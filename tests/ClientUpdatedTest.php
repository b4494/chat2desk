<?php

use BmPlatform\Chat2Desk\Utils\DataWrap;
use Mockery as m;

class ClientUpdatedTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testClientUpdatedEvent()
    {
        $dataJson = '{
  "id": 10379672,
  "phone": "[tg] 192107759",
  "name": "Alexander Kalnoy",
  "avatar": "https://storage.chat2desk.com/clients/avatars/2019-12/1653605-1C86060E7AC3E63C6.jpg",
  "client_source": null,
  "assigned_name": "Кальной Александр",
  "comment": "testing",
  "extra_comment_1": "Хорошо",
  "client_phone": "79205550123",
  "extra_comment_2": "test",
  "extra_comment_3": null,
  "channels": {
    "id": null,
    "transports": [
      null
    ]
  },
  "region_id": 2789,
  "country_id": 2,
  "custom_fields": {
    "1": "poka"
  },
  "hook_type": "client_updated",
  "event_time": "2022-03-21T14:05:59Z"
}';

        $data = m::mock(DataWrap::class, [ m::mock(\BmPlatform\Chat2Desk\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]), json_decode($dataJson, true)]);
        $data->expects('timestamp')->andReturn($ts = Carbon\Carbon::now());
        $data->makePartial();

        $eventHandler = new \BmPlatform\Chat2Desk\EventHandlers\ClientUpdated();

        $this->assertEquals(new \BmPlatform\Abstraction\Events\ContactDataUpdated(
            contact: new \BmPlatform\Abstraction\DataTypes\Contact(
                externalId: '10379672',
                name: 'Кальной Александр',
                phone: '79205550123',
                avatarUrl: 'https://storage.chat2desk.com/clients/avatars/2019-12/1653605-1C86060E7AC3E63C6.jpg',
                extraFields: [
                    '1' => 'poka',
                    'cm' => 'testing',
                ],
                extraData: [
                    'rgid' => 2789,
                    'ctid' => 2,
                    'clph' => '[tg] 192107759',
                ],
            ),
            timestamp: $ts,
        ), $eventHandler($data));
    }
}