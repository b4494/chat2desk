<?php

use BmPlatform\Abstraction\DataTypes\AppExternalStatus;
use BmPlatform\Abstraction\Enums\AppStatus;
use Illuminate\Support\Facades\Config;
use Mockery as m;

class IntegratorTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testIntegrates()
    {
        Config::expects('get')->with('c2d.servers')->andReturn([
            'ru' => $server = [ 'matcher' => 'https://ror\.chat2desk\.com', 'region_id' => 'RUS01' ],
        ]);

        Config::expects('get')->with('c2d.servers.ru')->andReturn($server);

        $integrator = m::mock(\BmPlatform\Chat2Desk\Integrator::class, [[
            'lang' => 'ru',
            'token' => 'token',
            'tz_offset' => 3,
            'api_domain' => 'https://ror.chat2desk.com',
        ], $billingStatusResolver = m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)]);

        $integrator->shouldAllowMockingProtectedMethods();
        $integrator->expects('createApiClient')->andReturn($apiClient = m::mock(\BmPlatform\Chat2Desk\ApiClient::class))->zeroOrMoreTimes();
        $integrator->makePartial();

        $billingStatusResolver->expects('resolveCompanyStatus')
            ->with('RUS01-1234')
            ->andReturn($status = new AppExternalStatus(AppStatus::Active));

        $billingStatusResolver->currency = 643;

        $apiClient->expects('get')->with('companies/api_info')->andReturn(json_decode('{
        "companyID": 1234,
        "partnerID": 30,
        "company_mode": "standard",
        "hook": null,
        "hook_preferences": null,
        "current_version": 1,
        "channels": 4,
        "company_name": "Company Name",
        "company_lang": "ru",
        "admin_email": "mail@example.com",
        "custom_info": null,
        "write_first_option": false,
        "domain_urls": [
            "https://web.chat2desk.com"
        ],
        "api_calls": {
            "used_this_month": 0,
            "left_free_requests": 99971,
            "left_paid_requests": 0
        }
    }', true));

        $apiClient->expects('get')->with('custom_client_fields')->andReturn(json_decode('[
        {
            "id": 1,
            "editable": true,
            "viewable": true,
            "name": "field 1",
            "type": "input",
            "value": ""
        },
        {
            "id": 2,
            "editable": true,
            "viewable": false,
            "name": "field 2",
            "type": "dropdown",
            "value": "option 1,option 2"
        }]', true));

        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\AppIntegrationData(
            type: 'c2d',
            externalId: 'ru:1234',
            name: 'Company Name',
            locale: 'ru',
            timeZone: \Carbon\CarbonTimeZone::createFromHourOffset(3),
            email: 'mail@example.com',
            extraData: [
                'token' => 'token',
                'writeFirst' => false,
                'customFields' => [
                    [ 'id' => 1, 'name' => 'field 1', 'type' => 'input' ],
                    [ 'id' => 2, 'name' => 'field 2', 'type' => 'options', 'options' => [ 'option 1', 'option 2' ] ],
                ],
                'billingId' => 'RUS01-1234',
            ],
            paymentType: \BmPlatform\Abstraction\Enums\PaymentType::External,
            status: $status,
            subPartnersChain: ['RUS01', 'RUS01_30'],
            currency: 'RUB',
        ), $integrator());

        $apiClient->expects('get')->with('companies/api_info')->andReturn(['company_mode'=>'demo']);
        try {
            $integrator();
            $this->fail('Should throw exception');
        }

        catch (\BmPlatform\Chat2Desk\Exceptions\ErrorException $e) {
            $this->assertEquals(\BmPlatform\Abstraction\Enums\ErrorCode::IntegrationNotPossible, $e->errorCode);
        }
    }
}