<?php

use BmPlatform\Chat2Desk\Utils\DataWrap;
use Mockery as m;

class CloseDialogTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testCloseDialogEvent()
    {
        $dataJson = '{
  "dialog_id": 48149439,
  "request_id": 195598761,
  "created": 1647775783,
  "updated": 1647862448,
  "channel_id": 81354,
  "client_id": 525777647,
  "operator_id": 243951,
  "current_operator_id": 243951,
  "transport": "telegram",
  "hook_type": "close_dialog",
  "event_time": "2022-03-21T11:34:08Z"
}';

        $data = m::mock(DataWrap::class, [ m::mock(\BmPlatform\Chat2Desk\AppHandler::class, [
            $u = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]), json_decode($dataJson, true)]);
        $data->expects('chat')->andReturn($chat = m::mock(\BmPlatform\Abstraction\DataTypes\Chat::class));
        $data->expects('timestamp')->andReturn($ts = Carbon\Carbon::now());
        $data->makePartial();

        $instance = new \BmPlatform\Chat2Desk\EventHandlers\CloseDialog();

        $this->assertEquals(new \BmPlatform\Abstraction\Events\ChatTicketClosed(
            chat: $chat,
            ticketId: '195598761',
            timestamp: $ts,
        ), $instance($data));
    }
}