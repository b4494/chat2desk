<?php

use Mockery as m;

class AppendCustomFieldsTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testCustomFieldsConversion(): void
    {
        $handler = m::mock(\BmPlatform\Chat2Desk\AppHandler::class, [
            $user = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class),
            m::mock(\BmPlatform\Chat2Desk\BillingCommands::class),
        ]);
        $user->expects('getExtraData')->andReturn([
            \BmPlatform\Chat2Desk\Utils\ExtraDataProps::CUSTOM_FIELDS => [
                [ 'id' => 1, 'name' => 'Product.ID', 'type' => 'input' ],
                [ 'id' => 2, 'name' => 'Целевое действие', 'type' => 'options', 'options' => [ 'true', 'false' ] ],
            ]
        ]);
        $user->expects('getExternalId')->andReturn(123);
        $func = new \BmPlatform\Chat2Desk\Processors\AppendCustomFields;

        $this->assertEquals([
            'customField1' => [ 'type' => 'text', 'isCustom' => true, 'title' => 'Product.ID', 'variableRef' => 'contact.customFields.Product ID' ],
            'customField2' => [ 'type' => 'enum', 'isCustom' => true, 'title' => 'Целевое действие', 'variableRef' => 'contact.customFields.Целевое действие', 'withLabels' => false, 'options' => [
                [ 'value' => 'true' ],
                [ 'value' => 'false' ],
            ]],
        ], $func($handler, []));
    }
}