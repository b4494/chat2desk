<?php

use BmPlatform\Abstraction\DataTypes\Chat;
use BmPlatform\Abstraction\DataTypes\MessengerInstance;
use BmPlatform\Abstraction\Events\AttachedTagToChat;
use BmPlatform\Abstraction\Interfaces\AppInstance;
use BmPlatform\Chat2Desk\AppHandler;
use Mockery as m;

class AddTagToRequestTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testAddTagToRequest()
    {
        $dataJson = '{
  "id": 18230,
  "name": "Не будет",
  "description": "не придёт на вебинар",
  "group_id": 3393,
  "group_name": "Туннели продаж",
  "client_id": 10379672,
  "request_id": 193774620,
  "channel_id": 19706,
  "transport": "telegram",
  "hook_type": "add_tag_to_request",
  "event_time": "2022-03-23T08:27:36Z"
}';
        $module = m::mock(AppHandler::class, [ $u = m::mock(AppInstance::class), m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)]);
        $data = m::mock(\BmPlatform\Chat2Desk\Utils\DataWrap::class, [ $module, json_decode($dataJson, true) ]);
        $data->expects('dialogId')->andReturn(1);
        $data->expects('timestamp')->andReturn($ts = \Carbon\Carbon::now());
        $data->makePartial();

        $handler = new \BmPlatform\Chat2Desk\EventHandlers\AddTagToRequest();

        $this->assertEquals(new AttachedTagToChat(
            chat: new Chat(1, '19706:telegram', contact: '10379672', extraData: [
                'clid' => 10379672,
                'reqid' => 193774620,
            ]),
            tag: '18230',
            forTicket: true,
            timestamp: $ts,
        ), $handler($data));
    }
}