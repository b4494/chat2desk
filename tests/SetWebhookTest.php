<?php

use Mockery as m;

class SetWebhookTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testSetsWebhook()
    {
        \Illuminate\Support\Facades\App::expects('isLocal')->andReturn(false)->zeroOrMoreTimes();

        \Illuminate\Support\Facades\URL::expects('route')->with('c2d.callback', [ 'callbackId' => 'hash' ])->andReturn('url')->zeroOrMoreTimes();

        $handler = m::mock(\BmPlatform\Chat2Desk\AppHandler::class, [
            $user = m::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            m::mock(\Illuminate\Contracts\Config\Repository::class), m::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]);
        $handler->expects('getApiClient')->andReturn($apiClient = m::mock(\BmPlatform\Chat2Desk\ApiClient::class))->zeroOrMoreTimes();

        $hub = m::mock(\BmPlatform\Abstraction\Interfaces\Hub::class);
        $hub->expects('getCallbackId')->with($user)->andReturn('hash')->zeroOrMoreTimes();

        $target = new \BmPlatform\Chat2Desk\SetWebhook($handler, $hub);

        $apiClient->expects('get')->with('webhooks')->andReturn([]);
        $apiClient->expects('post')->with('webhooks', [
            'json' => [
                'url' => 'url',
                'name' => 'Sales Tunnels',
                'status' => 'enable',
                'events' => \BmPlatform\Chat2Desk\SetWebhook::EVENTS,
            ],
        ]);

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            [ 'url' => 'url', 'status' => 'disable', 'id' => 1 ],
        ]);
        $apiClient->expects('put')->with('webhooks/:id', [
            'params' => [ 'id' => 1 ],
            'json' => [
                'url' => 'url',
                'name' => 'Sales Tunnels',
                'status' => 'enable',
                'events' => \BmPlatform\Chat2Desk\SetWebhook::EVENTS,
            ],
        ]);

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            [ 'url' => 'url', 'status' => 'enable', 'id' => 1, 'events' => \BmPlatform\Chat2Desk\SetWebhook::EVENTS ],
        ]);

        $target();

        $apiClient->expects('get')->with('webhooks')->andReturn([
            [ 'url' => 'url', 'status' => 'enable', 'id' => 1, 'events' => \BmPlatform\Chat2Desk\SetWebhook::EVENTS ],
        ]);
        $apiClient->expects('put')->with('webhooks/:id', [
            'params' => [ 'id' => 1 ],
            'json' => [
                'url' => 'url',
                'name' => 'Sales Tunnels',
                'status' => 'disable',
                'events' => \BmPlatform\Chat2Desk\SetWebhook::EVENTS,
            ],
        ]);

        $target(false);
    }
}