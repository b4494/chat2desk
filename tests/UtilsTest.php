<?php

use BmPlatform\Chat2Desk\Utils\Utils;

class UtilsTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    public function testMatchesServer()
    {
        \Illuminate\Support\Facades\Config::expects('get')->with('c2d.servers')->andReturn([
            'ru' => [ 'matcher' => 'https://example\.com' ],
        ])->times(2);

        $this->assertEquals('ru', Utils::matchServer('https://example.com'));
        $this->assertNull(Utils::matchServer('https://subdomain.example.com'));
    }

    public function testExtractsChannelTransportParams()
    {
        $mi = Mockery::mock(\BmPlatform\Abstraction\Interfaces\MessengerInstance::class);
        $mi->expects('getExternalId')->andReturn('1:telegram');

        $this->assertEquals([ 'channel_id' => 1, 'transport' => 'telegram' ], Utils::channelTransportParams($mi));
    }
}