<?php

use BmPlatform\Abstraction\DataTypes\InlineButton;
use BmPlatform\Abstraction\DataTypes\InlineUrlButton;
use BmPlatform\Abstraction\DataTypes\MediaFile;
use BmPlatform\Abstraction\DataTypes\QuickReply;
use BmPlatform\Abstraction\Enums\ButtonColor;
use BmPlatform\Abstraction\Enums\ErrorCode;
use BmPlatform\Abstraction\Enums\MediaFileType;
use BmPlatform\Abstraction\Enums\QuickReplyType;
use BmPlatform\Abstraction\Interfaces\Chat;
use BmPlatform\Abstraction\Interfaces\Contact;
use BmPlatform\Abstraction\Interfaces\MessengerInstance;
use BmPlatform\Abstraction\Requests\SendMediaRequest;
use BmPlatform\Abstraction\Requests\SendSystemMessageRequest;
use BmPlatform\Abstraction\Requests\SendTextMessageRequest;
use BmPlatform\Abstraction\Requests\TransferChatToOperatorRequest;
use BmPlatform\Abstraction\Requests\UpdateContactDataRequest;
use BmPlatform\Abstraction\Responses\MessageSendResult;
use BmPlatform\Abstraction\Responses\NewOperatorResponse;
use BmPlatform\Chat2Desk\ApiClient;
use BmPlatform\Chat2Desk\Commands;
use BmPlatform\Chat2Desk\AppHandler;
use BmPlatform\Chat2Desk\Exceptions\ValidationException;
use Illuminate\Support\MessageBag;
use Mockery\Adapter\Phpunit\MockeryTestCase;

class CommandsTest extends MockeryTestCase
{
    public function testSendsTextMessages()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'autoreply',
                'text' => 'Hello, World!',
            ],
        ])->andReturn([ 'message_id' => 1 ]);

        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'autoreply',
                'text' => 'Hello, World!',
                'keyboard' => [
                    'buttons' => [
                        [ 'type' => 'reply', 'text' => 'reply' ],
                        [ 'type' => 'phone', 'text' => 'phone', 'color' => 'blue' ],
                        [ 'type' => 'location', 'text' => 'location', 'color' => 'green' ],
                    ],
                ],
                'inline_buttons' => [
                    [ 'type' => 'url', 'url' => 'https://example.com', 'text'=>'url'],
                    [ 'type' => 'reply', 'text' => 'reply', 'payload' => 'payload', 'color' => 'red' ],
                ],
            ],
        ])->andReturn([ 'message_id' => 2 ]);

        $messenger = Mockery::mock(MessengerInstance::class);
        $messenger->expects('getExternalId')->zeroOrMoreTimes()->andReturn('1:transport');

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getMessengerInstance')->zeroOrMoreTimes()->andReturn($messenger);
        $chat->expects('getExtraData')->zeroOrMoreTimes()->andReturn([ 'clid' => 1 ]);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        $commands = new Commands($module);
        $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'Hello, World!'));
        $this->assertEquals(1, $resp->externalId);

        $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'Hello, World!',
         quickReplies: [
               [ new QuickReply('reply', color: ButtonColor::Secondary)],
                [new QuickReply('phone', QuickReplyType::Phone, color: ButtonColor::Primary)],
                [new QuickReply('location', QuickReplyType::Location, color: ButtonColor::Positive)],
            ],inlineButtons: [
                [new InlineUrlButton('url', 'https://example.com', 'payload')],
                [new InlineButton('reply', 'payload', ButtonColor::Negative)],
            ]
        ));

        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'autoreply',
                'text' => 'Hello, World!',
            ],
        ])->andThrow(new ValidationException(new MessageBag([
            'transport' => ['Client doesnt use transport insta_local'],
            'client_id' => ['Client #10379672 does not exist with specified transport or channel']
        ])));

        try {
            $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'Hello, World!'));
            $this->fail('expected ErrorException');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(ErrorCode::ChatNotFound, $e->errorCode);
        }

        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'autoreply',
                'text' => 'Hello, World!',
            ],
        ])->andThrow(new ValidationException(new MessageBag([
            'client_id' => ['Client #10379672 does not exist with specified transport or channel','Client #103796722 not found']
        ])));

        try {
            $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'Hello, World!'));
            $this->fail('expected ErrorException');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(ErrorCode::CustomerNotFound, $e->errorCode);
        }

        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'autoreply',
                'text' => 'Hello, World!',
            ],
        ])->andThrow(new ValidationException(new MessageBag([
            'channel_id' => ['Channel #23222 not found']
        ])));

        try {
            $resp = $commands->sendTextMessage(new SendTextMessageRequest($chat, 'Hello, World!'));
            $this->fail('expected ErrorException');
        }

        catch (\BmPlatform\Abstraction\Exceptions\ErrorException $e) {
            $this->assertEquals(ErrorCode::InvalidMessengerInstance, $e->errorCode);
        }

        $apiClient->expects('post')->with('messages', [
            'json' => [
                'client_id' => 1,
                'transport' => 'transport',
                'channel_id' => 1,
                'open_dialog' => false,
                'type' => 'system',
                'text' => 'system',
            ],
        ])->andReturn([ 'message_id' => 1 ]);

        $commands->sendSystemMessage(new SendSystemMessageRequest($chat, 'system'));
    }

    public function testFieldsUpdated()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $contact = Mockery::mock(Contact::class);
        $contact->expects('getExternalId')->andReturn(1)->times(4);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        $commands = new Commands($module);

        $apiClient->expects('put')->with('clients/:clientId', [
            'params' => [ 'clientId' => 1 ],
            'json' => [
                'nickname' => 'name',
                'assigned_phone' => '79205550123',
                'comment' => 'comment',
                'custom_fields' => [
                    '1' => 'field',
                ],
            ],
        ]);

        $contact->expects('getExtraFields')->andReturn([ '2' => 'test' ])->times(2);
        $contact->expects('getAvatarUrl')->andReturn('avatar')->times(2);
        $res = $commands->updateContactData(new UpdateContactDataRequest($contact, [
            'nickname' => 'name',
            'assigned_phone' => '+79205550123',
            'comment' => 'comment',
            'customField1' => 'field',
        ]));

        $this->assertEquals(new \BmPlatform\Abstraction\DataTypes\Contact(1, 'name', '79205550123', null, 'avatar', [
            '1' => 'field',
            '2' => 'test',
            'cm' => 'comment',
        ]), $res);

        $apiClient->expects('put')->with('clients/:clientId', [
            'params' => [ 'clientId' => 1 ],
            'json' => [
                'assigned_phone' => '79205550123',
            ],
        ]);

        $contact->expects('getName')->andReturn('name');
        $commands->updateContactData(new UpdateContactDataRequest($contact, [ 'assigned_phone' => '79205550123' ]));
    }

    public function testMediaFileSends()
    {
        $apiClient = Mockery::mock(ApiClient::class);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        $commands = new Commands($module);

        $chat = $this->mockChat();

        $req = new SendMediaRequest($chat, new MediaFile(MediaFileType::Image, 'image'), 'caption');
        $apiClient->expects('post')->with('messages', [
            'json' => [
                'type' => 'autoreply',
                'client_id' => 100,
                'channel_id' => 1,
                'transport' => 'telegram',
                'open_dialog' => false,
                'attachment' => 'image',
                'text' => 'caption',
            ],
        ])->andReturn([ 'message_id' => 100 ]);

        $this->assertEquals(new MessageSendResult(100), $commands->sendMediaMessage($req));

        // PDF
        $req = new SendMediaRequest($chat, new MediaFile(MediaFileType::Document, 'pdf'));
        $apiClient->expects('post')->with('messages', [
            'json' => [
                'type' => 'autoreply',
                'client_id' => 100,
                'channel_id' => 1,
                'transport' => 'telegram',
                'open_dialog' => false,
                'pdf' => 'pdf',
            ],
        ])->andReturn([ 'message_id' => 101 ]);

        $this->assertEquals(new MessageSendResult(101), $commands->sendMediaMessage($req));
    }

    public function testTransfersToOperator()
    {
        [ $apiCommands, $apiClient, $module ] = $this->mockModule();

        $chat = $this->mockChat();

        $operatorGroup = Mockery::mock(\BmPlatform\Abstraction\Interfaces\OperatorGroup::class);
        $operatorGroup->expects('getExternalId')->andReturn(1)->times(2);

        $commands = new Commands($module);

        $apiCommands->expects('messages')->with([
            'client_id' => 100,
            'channel_id' => 1,
            'transport' => 'telegram',
            'order' => 'desc',
            'limit' => 1,
        ])->andReturn([[ 'id' => 123 ]])->zeroOrMoreTimes();

        $apiClient->expects('get')->with('messages/:id/transfer_to_group', [
            'params' => [ 'id' => 123 ],
            'query' => [ 'group_id' => 1],
        ])->andReturn([ 'assignee' => [ 'id' => 10 ]]);

        $req = new TransferChatToOperatorRequest($chat, $operatorGroup);
        $this->assertEquals(new NewOperatorResponse('10'), $commands->transferChatToOperator($req));

        // With dialog
        $apiClient->expects('get')->with('messages/:id/transfer_to_group', [
            'params' => [ 'id' => 123 ],
            'query' => [ 'group_id' => 1],
        ])->andThrow(new \BmPlatform\Chat2Desk\Exceptions\ErrorException(ErrorCode::NotFound, 'Operator Group #123 not found'));

        try {
            $commands->transferChatToOperator($req);
            $this->fail('Must throw exception');
        }

        catch (\BmPlatform\Chat2Desk\Exceptions\ErrorException $e) {
            $this->assertEquals(ErrorCode::OperatorNotFound, $e->errorCode);
        }

        // Transfer dialog
        $apiClient->expects('get')->with('messages/:id/transfer', [
            'params' => [ 'id' => 123 ],
            'query' => [ 'operator_id' => 2 ],
        ]);

        $operator = Mockery::mock(\BmPlatform\Abstraction\Interfaces\Operator::class);
        $operator->expects('getExternalId')->andReturn(2)->times(2);

        $req = new TransferChatToOperatorRequest($chat, $operator);
        $this->assertEquals(new NewOperatorResponse('2'), $commands->transferChatToOperator($req));
    }

    public function testAttachesAndDetachesTags()
    {
        /** @var \Mockery\MockInterface $apiClient */
        [ $apiCommands, $apiClient, $module ] = $this->mockModule();

        $commands = new BmPlatform\Chat2Desk\Commands($module);

        $chat = $this->mockChat();

        $apiClient->expects('post')->with('tags/assign_to', [
            'json' => [
                'tag_ids' => [ 123 ],
                'assignee_type' => 'client',
                'assignee_id' => 100,
            ],
        ])->andReturn(true);

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, 123);

        $commands->attachTagToChat($req);

        $tag = Mockery::mock(\BmPlatform\Abstraction\Interfaces\Tag::class);
        $tag->expects('getExternalId')->andReturn('123')->zeroOrMoreTimes();

        $apiClient->expects('post')->with('tags/assign_to', [
            'json' => [
                'tag_ids' => [ 123 ],
                'assignee_type' => 'client',
                'assignee_id' => 100,
            ],
        ])->andReturn(true);

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, $tag);

        $commands->attachTagToChat($req);

        $apiClient->expects('post')->with('tags/assign_to', [
            'json' => [
                'tag_ids' => [ 123 ],
                'assignee_type' => 'request',
                'assignee_id' => 1000,
            ],
        ])->andReturn(true);

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, $tag, forTicket: true);
        $commands->attachTagToChat($req);

        // Delete tags
        $apiClient->expects('delete')->with('tags/:id/delete_from', [
            'params' => [ 'id' => 123 ],
            'json' => [
                'client_id' => 100,
            ],
        ])->andReturn(true);

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, $tag);
        $commands->detachTagFromChat($req);

        $apiClient->expects('delete')->with('tags/:id/delete_from', [
            'params' => [ 'id' => 123 ],
            'json' => [
                'request_id' => 1000,
            ],
        ])->andReturn(true);

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, $tag, forTicket: true);
        $commands->detachTagFromChat($req);

        // Doesnt throw exception
        $apiClient->expects('delete')->with('tags/:id/delete_from', [
            'params' => [ 'id' => 123 ],
            'json' => [
                'client_id' => 100,
            ],
        ])->andThrow(new ValidationException(new MessageBag([
            'assignee_id' => 'The client not found',
        ])));

        $req = new \BmPlatform\Abstraction\Requests\ChatTagRequest($chat, $tag);
        $commands->detachTagFromChat($req);
    }

    /**
     * @return \BmPlatform\Abstraction\Interfaces\Chat|\Mockery\LegacyMockInterface|\Mockery\MockInterface
     */
    protected function mockChat(): \Mockery\LegacyMockInterface|Chat|\Mockery\MockInterface
    {
        $mi = Mockery::mock(MessengerInstance::class);
        $mi->expects('getExternalId')->andReturn('1:telegram')->zeroOrMoreTimes();

        $chat = Mockery::mock(Chat::class);
        $chat->expects('getExternalId')->andReturn(1)->zeroOrMoreTimes();
        $chat->expects('getMessengerInstance')->andReturn($mi)->zeroOrMoreTimes();
        $chat->expects('getExtraData')->andReturn([
            'clid' => 100,
            'opid' => null,
            'reqid' => 1000,
        ])->zeroOrMoreTimes();

        return $chat;
    }/**
 * @return array
 */
    protected function mockModule(): array
    {
        $apiCommands = Mockery::mock(\BmPlatform\Chat2Desk\ApiCommands::class);

        $apiClient = Mockery::mock(ApiClient::class);

        $module = Mockery::mock(AppHandler::class);
        $module->expects('getApiCommands')->andReturn($apiCommands)->zeroOrMoreTimes();
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        return array( $apiCommands, $apiClient, $module );
    }

    public function testChatTicketClosed()
    {
        $apiClient = Mockery::mock(ApiClient::class);
        $module = Mockery::mock(AppHandler::class, [
            $user = Mockery::mock(\BmPlatform\Abstraction\Interfaces\AppInstance::class),
            Mockery::mock(\Illuminate\Contracts\Config\Repository::class), Mockery::mock(\BmPlatform\Chat2Desk\BillingCommands::class)
        ]);
        $module->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        \Illuminate\Support\Facades\Event::expects('dispatch')->times(2);

        $chat = $this->mockChat();
        $commands = new BmPlatform\Chat2Desk\Commands($module);

        $apiClient->expects('get')->with('dialogs/:id', [
            'params' => [ 'id' => 1 ],
        ])->andReturn([
            'state' => 'open',
            'operator_id' => 2,
        ]);

        $apiClient->expects('put')->with('dialogs/:id', [
            'params' => [ 'id' => 1 ],
            'json' => [ 'state' => 'closed', 'operator_id' => 2 ],
        ]);

        $commands->closeChatTicket($chat);

        // Close already closed dialog
        $apiClient->expects('get')->with('dialogs/:id', [
            'params' => [ 'id' => 1 ],
        ])->andReturn([
            'state' => 'closed',
            'operator_id' => 2,
        ]);

        try {
            $commands->closeChatTicket($chat);
            $this->fail('Should throw exception');
        }

        catch (\BmPlatform\Chat2Desk\Exceptions\ErrorException $e) {
            $this->assertEquals(ErrorCode::ChatTicketAlreadyClosed, $e->errorCode);
        }

        // Close without operator assigned
        $apiClient->expects('get')->with('dialogs/:id', [
            'params' => [ 'id' => 1 ],
        ])->andReturn([
            'state' => 'open',
            'operator_id' => null,
        ]);

        $user->expects('getExtraData')->andReturn([
            'admid' => 1000,
        ]);

        $apiClient->expects('put')->with('dialogs/:id', [
            'params' => [ 'id' => 1 ],
            'json' => [ 'state' => 'closed', 'operator_id' => 1000 ],
        ]);

        $commands->closeChatTicket($chat);
    }

    public function testCustomApiCall(): void
    {
        $handler = Mockery::mock(\BmPlatform\Chat2Desk\AppHandler::class);
        $commands = new BmPlatform\Chat2Desk\Commands($handler);

        $context = Mockery::mock(\BmPlatform\Abstraction\Interfaces\RuntimeContext::class);

        $apiClient = Mockery::mock(ApiClient::class);
        $apiClient->expects('request')->with('GET', 'clients', [ 'query' => [ 'param' => 'value' ]])->andReturn('data');

        $handler->expects('getApiClient')->andReturn($apiClient)->zeroOrMoreTimes();

        $this->assertEquals([
            'ok' => true,
            'data' => 'data',
        ], $commands->callCustomAction($context, 'c2dApiCall', [
            'type' => 'GET',
            'path' => 'clients',
            'params' => [ 'param' => 'value' ],
        ]));

        $apiClient->expects('request')->with('POST', 'clients', [ 'json' => [ 'param' => 'value' ]])->andReturn('data');
        $this->assertEquals(['ok' => true, 'data'=>'data'], $commands->callCustomAction($context, 'c2dApiCall', [
            'type' => 'POST',
            'path' => 'clients',
            'params' => [ 'param' => 'value' ],
        ]));

        $apiClient->expects('request')->with('PUT', 'clients', [ 'json' => null ])->andThrow(new ValidationException(new MessageBag([
            'field' => [ 'error' ],
        ])));
        $this->assertEquals(['ok'=>false,'message'=>'','errors'=>['error']],$commands->callCustomAction($context, 'c2dApiCall', [
            'type' => 'PUT',
            'path' => 'clients',
            'params' => null,
        ]));
    }
}