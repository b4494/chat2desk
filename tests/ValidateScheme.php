<?php

use BmPlatform\Support\SchemaValidator;

class ValidateScheme extends \PHPUnit\Framework\TestCase
{
    public function testSchema(): void
    {
        $schema = require __DIR__ . '/../config/c2d.php';

        $this->assertNull((new SchemaValidator())($schema));
    }
}