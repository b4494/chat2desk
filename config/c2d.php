<?php return [
    'servers' => [
        'ru' => [
            'api_domain' => 'https://ror.chat2desk.com',
            'front_domain' => 'https://web.chat2desk.com',
            'matcher' => 'https://(api|ror)\.chat2desk\.com',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'RUS01',
            'timezone' => 'Europe/Moscow',
        ],

        'support' => [
            'api_domain' => 'https://ror-support.chat2desk.com',
            'front_domain' => 'https://support.chat2desk.com',
            'matcher' => 'https://(ror-support)\.chat2desk\.com',
            'locale' => 'ru',
            'currency' => 'RUB',
        ],

        'test20' => [
            'api_domain' => 'https://ror-test20.chat2desk.com',
            'front_domain' => 'https://test20.chat2desk.com',
            'matcher' => 'https://ror-test20\.chat2desk\.com',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'TEST',
        ],

        'test7' => [
            'api_domain' => 'https://ror-test7.chat2desk.com',
            'front_domain' => 'https://test7.chat2desk.com',
            'matcher' => 'https://ror-test7\.chat2desk\.com',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'TEST',
        ],

        'kaz-01' => [
            'api_domain' => 'https://ror.chat2desk.kz',
            'matcher' => 'https://(api|ror)\.chat2desk\.kz',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'KAZ01',
        ],

        'kaz-02' => [
            'api_domain' => 'https://ror-02.chat2desk.kz',
            'matcher' => 'https://ror-02\.chat2desk\.kz',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'KAZ02',
        ],

        'kaz-04' => [
            'api_domain' => 'https://ror-04.chat2desk.kz',
            'matcher' => 'https://ror-04\.chat2desk\.kz',
            'locale' => 'ru',
            'currency' => 'RUB',
            'region_id' => 'KAZ04',
        ],

        'mex-01' => [
            'api_domain' => 'https://ror.chat2desk.com.mx',
            'matcher' => 'https://ror\.chat2desk\.com\.mx',
            'locale' => 'es',
            'currency' => 'USD',
            'region_id' => 'MEX01',
        ],

        'eur-01' => [
            'api_domain' => 'https://api.chat24.io',
            'matcher' => 'https://api\.chat24\.io',
            'locale' => 'en',
            'currency' => 'EUR',
            'region_id' => 'EUR01',
        ],

        ...json_decode(env('C2D_EXTRA_SERVERS', '[]'), true),
    ],

    'billing' => [
        'username' => env('C2D_BILLING_USERNAME'),
        'password' => env('C2D_BILLING_PASSWORD'),
        'domain' => env('C2D_BILLING_DOMAIN', 'https://api.c2d.billingsaas.ru'),
        'options' => [
            // GuzzleHttp options
        ],
        'serviceIds' => [ 111, 122, 123, 124 ],
        'defaultOneTimeServiceId' => 415,
    ],

    'features' => [
        'operators' => [
            'read' => true,
            'transfer' => true,
        ],

        'operatorGroups' => [
            'read' => true,
            'transfer' => true,
        ],

        'tags' => [
            'read' => true,
            'create' => true,
            'attach' => true,
            'detach' => true,
            'attachForTicket' => true,
            'detachForTicket' => true,
        ],
    ],

    'actions' => [
        'send_text' => [
            'open_ticket' => true,
        ],

        'send_system_message' => [
            'supported' => true,
            'max_length' => 1000,
        ],

        'update_contact_data' => [
            'supported' => true,

            'editableFields' => [
                'type' => 'object',
                'properties' => [
                    'name' => [
                        'type' => 'string',
                        'title' => 'c2d::fields.contact_name',
                    ],

                    'assigned_phone' => [
                        'type' => 'phone',
                        'title' => 'c2d::fields.contact_phone',
                    ],

                    'comment' => [
                        'type' => 'text',
                        'title' => 'c2d::fields.contact_comment',
                    ],

                    'external_service' => [
                        'type' => 'enum',
                        'title' => 'c2d::fields.contact_external_service',
                        'options' => [
                            [ 'value' => 'amo', 'label' => 'AmoCRM' ],
                            [ 'value' => 'bitrix', 'label' => 'Bitrix24' ],
                        ],
                    ],

                    'external_id' => [
                        'type' => 'string',
                        'title' => 'c2d::fields.contact_external_id',
                    ],
                ],

                'processor' => \BmPlatform\Chat2Desk\Processors\AppendCustomFields::class,
            ],
        ]
    ],

    'custom_actions' => [
        [
            'id' => 'c2dApiCall',
            'title' => 'REST API',
            'icon' => 'house-gear',
            'args' => [
                'type' => 'object',
                'properties' => [
                    'type' => [
                        'type' => 'enum',
                        'title' => 'c2d::fields.api_request_type',
                        'options' => [
                            [ 'value' => 'GET', 'label' => 'GET' ],
                            [ 'value' => 'POST', 'label' => 'POST' ],
                            [ 'value' => 'PUT', 'label' => 'PUT' ],
                            [ 'value' => 'DELETE', 'label' => 'DELETE' ],
                        ],
                    ],

                    'path' => [
                        'type' => 'string',
                        'title' => 'c2d::fields.api_path',
                        'example' => 'clients',
                        'autocomplete' => [
                            'channels',
                            'channels/@{channelId}/clients',
                            'clients',
                            'clients/@{contact.id}',
                            'clients/@{contact.id}/questions',
                            'clients/@{contact.id}/transport',
                            'companies',
                            'companies/api_info',
                            'companies/info',
                            'companies/switch_mode',
                            'custom_client_fields',
                            'dialogs',
                            'help/api_modes',
                            'help/countries',
                            'help/dialog_states',
                            'help/message_types',
                            'help/roles',
                            'help/tickets',
                            'help/transports',
                            'messages/@{message.externalId}/read',
                            'messages/@{message.externalId}/transfer',
                            'messages/@{message.externalId}/unread',
                            'messages/import',
                            'messages/inbox',
                            'operators/statuses',
                            'qr_decode',
                            'regions',
                            'requests',
                            'requests/@{requestId}',
                            'requests/@{requestId}/messages',
                            'requests/close',
                            'scenarios/menu_items',
                            'scenarios/send_menu_item',
                            'statistics',
                            'tag_groups',
                            'tags',
                            'tags/assign_to',
                            'templates',
                            'templates_groups',
                            'tickets',
                            'web_analytics_data',
                            'web_hook',
                            'messages',
                        ]
                    ],

                    'params' => [
                        'type' => 'keyValue',
                        'title' => 'c2d::fields.api_params',
                        'description' => 'c2d::fields.api_params_description',
                    ],
                ],
                'required' => [ 'type', 'path' ],
            ],

            'result' => [
                'type' => 'object',
                'properties' => [
                    'data' => [ 'type' => 'mixed', 'title' => 'c2d::fields.response_data' ],
                    'ok' => [ 'type' => 'bool', 'title' => 'c2d::fields.response_is_ok' ],
                    'message' => [ 'type' => 'text', 'title' => 'c2d::fields.response_message' ],
                    'errors' => [ 'type' => 'text', 'array' => true, 'title' => 'c2d::fields.response_validation_errors' ],
                ],
            ],
        ]
    ],

    'events' => [
        'inboxFlags' => [
            'newChatCreated' => true,
            'newTicketOpened' => true,
            'externalPostComment' => true,
        ],
        'outboxSent' => true,
        'newContactChatAdded' => true,
        'chatTicketClosed' => true,
        'chatTransferred' => true,
        'chatTagAttached' => true,
        'chatTagDetached' => true,
        'chatTicketTagAttached' => true,
        'chatTicketTagDetached' => false,
        'messageStatusChanged' => true,
        'contactDataUpdated' => true,
        'externalItem' => true,
    ],

    'data' => [
        'integration' => [
            'locale' => true,
            'timezone' => true,
            'currency' => true,
        ],

        'contact' => [
            'phone' => true,
            'messengerId' => true,
        ],

        'chat' => [
            'messengerId' => true,
        ],

        'operator' => [
            'email' => true,
        ],
    ],

    'messenger_types' => [
        'default' => [
            'name' => 'Default settings',

            'text' => [
                'max_length' => 4000,
            ],

            'image' => [
                'enabled' => true,
                'mime' => ['image/jpeg','image/png','image/gif'],
                'max_file_size' => 5 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'video' => [
                'enabled' => true,
                'mime' => ['video/mp4', 'video/3gpp'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'document' => [
                'enabled' => true,
                'mime' => ['*'],
                'max_file_size' => 20 * 1024 * 1024,
                'caption' => true,
                'caption_max_length' => 4096,
            ],

            'audio' => [
                'enabled' => true,
                'mime' => ['audio/ogg','audio/mpeg','audio/mp4','audio/aac'],
                'max_file_size' => 20 * 1024 * 1024,
            ],

            'voice' => [
                'enabled' => true,
                'mime' => ['audio/ogg'],
                'max_file_size' => 1024 * 1024,
            ],

            'geolocation' => [ 'enabled' => false ],
        ],

        'with_buttons' => [
            'extends' => 'default',
            'name' => 'Messenger with buttons',

            'inline_buttons' => [
                'text' => true,
                'text_max_length' => 20,
                'url' => true,
                'payload' => true,
                'payload_max_length' => 256,
                'colors' => true,
                'max_count' => 32,
            ],

            'quick_replies' => [
                'text' => true,
                'text_max_length' => 20,
                'max_count' => 32,
            ],
        ],

        'telegram' => [
            'extends' => 'with_buttons',
            'name' => 'Telegram',
            'internal_type' => 'telegram',
            'quick_replies' => [
                'phone' => true,
                'geolocation' => true,
            ],
        ],

        'whatsapp' => [
            'extends' => 'default',
            'name' => 'Whatsapp',
            'internal_type' => 'whatsapp',
        ],

        'wa_with_buttons' => [
            'extends' => 'default',
            'name' => 'Whatsapp With Buttons',
            'internal_type' => 'whatsapp',

            'inline_buttons' => [
                'text' => true,
                'text_max_length' => 20,
                'payload' => true,
                'payload_max_length' => 256,
                'max_count' => 3,
            ],
        ],

        'wa_clickatell' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (clickatell)' ],
        'wa_infobip' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (infobip)' ],
        'wa_cm' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (cm)' ],
        'wa_wavy' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (wavy)' ],
        'wa_botmaker' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (botmaker)' ],
        'wa_ba_dialog' => [ 'extends' => 'wa_with_buttons', 'name' => 'Whatsapp (ba dialog)' ],
        'wa_dialog' => [ 'extends' => 'wa_with_buttons', 'name' => 'Whatsapp (dialog)' ],
        'wa_tr' => [ 'extends' => 'whatsapp', 'name' => 'Whatsapp (tr)' ],
        'wa_gupshup' => [ 'extends' => 'wa_with_buttons', 'name' => 'Whatsapp (gupshup)' ],
        'wa_direct' => [ 'extends' => 'wa_with_buttons', 'name' => 'Whatsapp (direct)' ],

        'viber' => [
            'extends' => 'default',
            'name' => 'Viber',
        ],

        'viber_infobip' => [ 'extends' => 'viber', 'name' => 'Viber (infobip)' ],
        'viber_tr' => [ 'extends' => 'viber', 'name' => 'Viber (tr)' ],
        'viber_devino' => [ 'extends' => 'viber', 'name' => 'Viber (devino)' ],

        'viber_business' => [
            'extends' => 'default',
            'name' => 'Viber Business',
        ],

        'viber_public' => [
            'extends' => 'with_buttons',
            'name' => 'Viber Public',
            'internal_type' => 'viber_public',
            'inline_buttons' => [
                'colors' => true,
            ],
            'quick_replies' => [
                'colors' => true,
            ],
        ],

        'facebook' => [
            'extends' => 'with_buttons',
            'name' => 'Facebook',
            'internal_type' => 'facebook',
            'quick_replies' => [
                'phone' => true,
            ],
        ],

        'widget' => [
            'extends' => 'with_buttons',
            'name' => 'Online Chat',
        ],

        'vk' => [
            'extends' => 'with_buttons',
            'internal_type' => 'vk',
            'name' => 'Вконтакте',
            'inline_buttons' => [
                'colors' => true,
                'url' => false,
            ],
            'quick_replies' => [
                'colors' => true,
            ],
        ],

        'external' => [
            'extends' => 'default',
            'name' => 'External channel',
        ],

        'instagram' => [
            'extends' => 'default',
            'name' => 'Instagram',
            'internal_type' => 'instagram',
        ],

        'insta_direct' => [ 'extends' => 'instagram', 'name' => 'Instagram (direct)' ],
        'ext_insta_direct' => [ 'extends' => 'instagram', 'name' => 'Instagram (ext direct)' ],
        'insta_i2crm' => [ 'extends' => 'instagram', 'name' => 'Instagram (i2crm)' ],
        'insta_local' => [ 'extends' => 'instagram', 'name' => 'Instagram (local)' ],

        'insta_official' => [
            'extends' => 'instagram',
            'name' => 'Instagram official',
            'internal_type' => 'instagram',
            'image' => [ 'enabled' => false ],
            'video' => [ 'enabled' => false ],
            'voice' => [ 'enabled' => false ],
            'audio' => [ 'enabled' => false ],
            'document' => [ 'enabled' => false ],
            'quick_replies' => [
                'text' => true,
                'text_max_length' => 20,
                'max_count' => 32,
            ],
        ],

        'tg_user' => [
            'extends' => 'default',
            'name' => 'Telegram User',
        ],

        'ok' => [ 'extends' => 'default', 'name' => 'Одноклассники' ],
        'skype' => [ 'extends' => 'default', 'name' => 'Skype' ],
        'email' => [ 'extends' => 'default', 'name' => 'E-mail' ],
        'twitter' => [ 'extends' => 'default', 'name' => 'Twitter' ],
        'wechat' => [ 'extends' => 'default', 'name' => 'WeChat' ],
        'yandex_dialogs' => [ 'extends' => 'default', 'name' => 'Yandex Dialogs' ],
        'avito' => [ 'extends' => 'default', 'name' => 'Avito' ],
        'drom' => [ 'extends' => 'default', 'name' => 'Drom' ],
    ],
];